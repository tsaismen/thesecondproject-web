using DeliverIt.Data;
using DeliverIT.Services.Services;
using DeliverIT.Services.Services.ServicesInterfaces;
using DeliverIT.Services.Tools;
using DeliverIT.Web.Helpers;
using DeliverIT.Web.Helpers.HelpersContracts;
using DeliverIT.Web.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace AspNetCoreDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddSwaggerGen(options =>
            //{
            //    options.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
            //    {
            //        Title = "DeliverIT Web API",
            //        Description = "Delivering packages to where you normaly cant.",
            //        Version = "v1"
            //    });
            //    var fileName = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            //    var filePath = Path.Combine(AppContext.BaseDirectory, fileName);
            //    options.IncludeXmlComments(filePath);
            //});

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromSeconds(1000);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            services.AddDistributedMemoryCache();

            services.AddControllersWithViews();
            services.AddDbContext<IDatabase, DeliveryDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped<IModelMapper, ModelMapper>();
            services.AddScoped<IAuthHelper, AuthHelper>();
            services.AddScoped<UniqueValidators>();
            services.AddScoped<ICityService, CityService>();
            services.AddScoped<ICountryService, CountrysService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddScoped<IParcelService, ParcelService>();
            services.AddScoped<IShipmentService, ShipmentService>();
            services.AddScoped<IWarehouseService, WarehouseService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSession();

            app.UseBrowserLink();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });

            //app.UseSwagger();
            //app.UseSwaggerUI(options =>
            //{
            //    options.SwaggerEndpoint("/swagger/v1/swagger.json", " DeliverIT API");
            //    options.RoutePrefix = "";
            //});
        }
    }
}