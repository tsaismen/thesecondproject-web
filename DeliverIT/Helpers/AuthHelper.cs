﻿using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using DeliverIT.Web.Helpers.HelpersContracts;

namespace DeliverIT.Web.Helpers
{
    public class AuthHelper : IAuthHelper
    {
        private readonly ICustomerService customerService;
        private readonly IEmployeeService employeesService;

        public AuthHelper(ICustomerService customerService, IEmployeeService employeesService)
        {
            this.customerService = customerService;
            this.employeesService = employeesService;
        }
        public bool MakeCustomerOrEmployeeValidation(int? id, string authentication)
        {
            if (authentication == null)
            {
                return false;
            }
            var registeredCustomer = TryGetCustomer(authentication);
            var registeredEmployee = TryGetEmployee(authentication);
            if (registeredEmployee.Email == null && registeredCustomer.Email == null)
            {
                return false;
            }
            else if (registeredCustomer.Email != null)
            {
                if (registeredCustomer.Id != id)
                {
                    return false;
                }
            }
            return true;
        }
        public bool MakeEmployeeValidation(string authentication)
        {
            if (authentication == null)
            {
                return false;
            }
            var employee = TryGetEmployee(authentication);
            if (employee.Email == null)
            {
                return false;
            }
            return true;
        }
        public MockUser TryGetCustomer(string credentials)
        {
            var customer = customerService.GetSingleCustomerByEmail(credentials);
            if (customer != null)
            {
                var user = new MockUser()
                {
                    Id = customer.CustomerId,
                    Email = customer.Email,
                    Role = "Customer"
                };
                return user;
            }
            else
            {
                var nulluser = new MockUser();
                return nulluser;
            }
        }
        public MockUser TryGetEmployee(string credentials)
        {
            var employee = employeesService.GetSingleEmployeeByEmail(credentials);
            if (employee != null)
            {
                var user = new MockUser()
                {
                    Id = employee.EmployeeId,
                    Email = employee.Email,
                    Role = "Employee"
                };
                return user;
            }
            else
            {
                var nulluser = new MockUser();
                return nulluser;
            }
        }


        public MockUser TryGetUser(string email, string password)
        {
            var customer = customerService.GetSingleCustomerByEmail(email);

            var employee = employeesService.GetSingleEmployeeByEmail(email);

            if (customer != null)
            {
                if (customer.Password != password)
                {
                    throw new AuthenticationException("Wrong password! Try again!");
                }

                var user = new MockUser()
                {
                    Id = customer.CustomerId,
                    Email = customer.Email,
                    Role = "Customer"
                };
                return user;
            }
            else if (employee != null)
            {
                if (employee.Password != password)
                {
                    throw new AuthenticationException("Wrong password! Try again!");
                }

                var user = new MockUser()
                {
                    Id = employee.EmployeeId,
                    Email = employee.Email,
                    Role = "Employee"
                };
                return user;
            }
            else
            {
                throw new AuthenticationException("User with that email doesn't exist!");
            }
        }

        public bool Exist(string userEmail)
        {
            var customer = customerService.GetSingleCustomerByEmail(userEmail);

            var employee = employeesService.GetSingleEmployeeByEmail(userEmail);

            if (customer != null || employee != null)
            {
                return true;
            }

            return false;
        }
    }
}
