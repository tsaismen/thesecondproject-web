﻿namespace DeliverIT.Web.Helpers.HelpersContracts
{
    public interface IAuthHelper
    {
        public bool MakeCustomerOrEmployeeValidation(int? id, string authentication);
        public bool MakeEmployeeValidation(string authentication);
        public MockUser TryGetCustomer(string credentials);
        public MockUser TryGetEmployee(string credentials);
        public MockUser TryGetUser(string email, string password);
        public bool Exist(string userEmail);
    }
}
