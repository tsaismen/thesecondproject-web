﻿using DeliverIT.Models;
using DeliverIT.Web.Models.ViewModels;

namespace DeliverIT.Web.Helpers.HelpersContracts
{
    public interface IModelMapper
    {
        public Customer ToModel(CustomerWeb customerWeb);
        public Employee ToModel(EmployeeWeb employeeWeb);
        public Parcel ToModel(ParcelWeb parcelWeb);
        public Shipment ToModel(ShipmentWeb shipmentWeb);
        public Warehouse ToModel(WarehouseWeb warehouseWeb);

        public CustomerViewModel ToViewModel(Customer customer);
        public EmployeeViewModel ToViewModel(Employee employee);
        public ParcelViewModel ToViewModel(Parcel parcel);
        public WarehouseViewModel ToViewModel(Warehouse warehouse);
        public ShipmentViewModel ToViewModel(Shipment shipment);
    }
}
