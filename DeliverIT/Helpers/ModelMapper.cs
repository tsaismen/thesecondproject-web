﻿using DeliverIT.Models;
using DeliverIT.Services.Services.ServicesInterfaces;
using DeliverIT.Web.Helpers.HelpersContracts;
using DeliverIT.Web.Models.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Web.Models
{
    public class ModelMapper : IModelMapper
    {
        private readonly IParcelService parcelService;

        public ModelMapper(IParcelService parcelService)
        {
            this.parcelService = parcelService;
        }
        public Customer ToModel(CustomerWeb customerWeb)
        {
            var address = new Address()
            {
                Street = customerWeb.Street,
                CityId = customerWeb.CityId,
                CountryId = customerWeb.CountryId
            };
            var customer = new Customer
            {
                FirstName = customerWeb.FirstName,
                LastName = customerWeb.LastName,
                Email = customerWeb.Email,
                Address = address,
                Password = customerWeb.Password
            };
            return customer;
        }
        public Employee ToModel(EmployeeWeb employeeWeb)
        {
            var address = new Address();
            if (employeeWeb.Street != null && employeeWeb.CityId != 0 && employeeWeb.CountryId != 0)
            {
                address.Street = employeeWeb.Street;
                address.CityId = employeeWeb.CityId;
                address.CountryId = employeeWeb.CountryId;
            }
            else
            {
                address = null;
            }
            var employee = new Employee
            {
                FirstName = employeeWeb.FirstName,
                LastName = employeeWeb.LastName,
                Email = employeeWeb.Email,
                Address = address,
                Password = employeeWeb.Password,
                WarehouseId = employeeWeb.WarehouseId
            };

            return employee;
        }
        public Parcel ToModel(ParcelWeb parcelWeb)
        {
            var parcel = new Parcel
            {
                WarehouseId = parcelWeb.WarehouseId,
                Weight = parcelWeb.Weight,
                CategoryId = parcelWeb.CategoryId,
                CustomerId = parcelWeb.CustomerId,
                HomeDelivery = parcelWeb.HomeDelivery,
                ShipmentId = parcelWeb.ShipmentId
            };

            return parcel;
        }
        public Shipment ToModel(ShipmentWeb shipmentWeb)
        {
            var parcelList = new List<Parcel>();
            if (shipmentWeb.Status == Status.Preparing || shipmentWeb.Status == Status.OnTheWay)
            {
                if (shipmentWeb.Parcels != null)
                {
                    foreach (var parcel in shipmentWeb.Parcels)
                    {
                        var parcelModel = parcelService.Get(int.Parse(parcel));
                        parcelModel.ShipmentId = shipmentWeb.ShipmentId;
                        parcelList.Add(parcelModel);
                    }
                }
            }
            var shipment = new Shipment
            {
                ArrivalDate = shipmentWeb.ArrivalDate,
                DepartureDate = shipmentWeb.DepartureDate,
                OriginWarehouseId = shipmentWeb.OriginId,
                DestinationWarehouseId = shipmentWeb.DestinationId,
                Status = shipmentWeb.Status,
                parcels = parcelList
            };

            return shipment;
        }

        public Warehouse ToModel(WarehouseWeb warehouseWeb)
        {
            var address = new Address()
            {
                Street = warehouseWeb.Street,
                CityId = warehouseWeb.CityId,
                CountryId = warehouseWeb.CountryId
            };
            var warehouse = new Warehouse
            {
                Address = address
            };
            return warehouse;
        }



        public ParcelViewModel ToViewModel(Parcel parcel)
        {
            var parcelView = new ParcelViewModel()
            {
                WarehouseId = parcel.WarehouseId,
                Weight = parcel.Weight,
                CategoryId = parcel.CategoryId,
                CustomerId = parcel.CustomerId,
                HomeDelivery = parcel.HomeDelivery,
                ShipmentId = parcel.ShipmentId
            };

            return parcelView;
        }
        public WarehouseViewModel ToViewModel(Warehouse warehouse)
        {
            var warehouseView = new WarehouseViewModel()
            {

                Street = warehouse.Address.Street,
                CityId = warehouse.Address.CityId,
                CountryId = warehouse.Address.CountryId
            };

            return warehouseView;
        }
        public ShipmentViewModel ToViewModel(Shipment shipment)
        {
            var shipmentView = new ShipmentViewModel()
            {
                ShipmentId = shipment.ShipmentId,
                OriginId = shipment.OriginWarehouseId,
                DestinationId = shipment.DestinationWarehouseId,
                DepartureDate = shipment.DepartureDate,
                ArrivalDate = shipment.ArrivalDate,
                ShipmentParcels = shipment.parcels.Select(x => new SelectListItem()
                {
                    Value = x.ParcelId.ToString(),
                    Text = $"ID:'{x.ParcelId}' , Customer:'{x.Customer.FirstName} {x.Customer.LastName}' , Category:'{x.Category.Name}'"
                }).ToList()
            };

            return shipmentView;
        }

        public CustomerViewModel ToViewModel(Customer customer)
        {
            var viewModel = new CustomerViewModel
            {
                CustomerId = customer.CustomerId,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Street = customer.Address.Street,
                Email = customer.Email,
                Address = customer.Address.ToString()
            };
            return viewModel;
        }
        public EmployeeViewModel ToViewModel(Employee employee)
        {
            var viewModel = new EmployeeViewModel
            {
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                Email = employee.Email,
                Street = employee.Address == null ? null : employee.Address.Street,
                CityId = employee.Address == null ? 0 : employee.Address.CityId,
                CountryId = employee.Address == null ? 0 : employee.Address.CountryId,
                WarehouseId = employee.WarehouseId,
                Password = employee.Password,
            };
            return viewModel;
        }
    }
}
