﻿using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using DeliverIT.Web.Helpers;
using DeliverIT.Web.Helpers.HelpersContracts;
using DeliverIT.Web.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Web.Controllers
{
    public class CustomerController : Controller
    {
        private readonly ICustomerService customerService;
        private readonly IModelMapper modelMapper;
        private readonly IAuthHelper authHelper;
        private readonly ICityService cityService;
        private readonly ICountryService countryService;

        public CustomerController(ICustomerService customerService, IModelMapper modelMapper, IAuthHelper authHelper, ICityService cityService, ICountryService countryService)
        {
            this.customerService = customerService;
            this.modelMapper = modelMapper;
            this.authHelper = authHelper;
            this.cityService = cityService;
            this.countryService = countryService;
        }
        [MyAuthorisationAttribute(Role = "Customer")]
        public IActionResult Index()
        {
            var customerId = int.Parse(this.HttpContext.Session.GetString("UserId"));
            var customer = customerService.GetById(customerId);
            var user = modelMapper.ToViewModel(customer);
            return View(user);
        }
        [MyAuthorisationAttribute(Role = "Customer")]
        public IActionResult Edit(int id)
        {
            try
            {
                var customer = this.customerService.GetById(id);
                var customerViewModel = this.modelMapper.ToViewModel(customer);

                return this.EditView(customerViewModel);
            }
            catch (EntityNotFoundException)
            {
                return this.View("Error");
            }
        }
        [MyAuthorisationAttribute(Role = "Customer")]
        [HttpPost]
        public IActionResult Edit(int id, [Bind("FirstName, LastName, Email, Password, Street,CityId,CountryId,ConfirmPassword,")] CustomerViewModel registerViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(registerViewModel);
            }

            if (!registerViewModel.Password.Equals(registerViewModel.ConfirmPassword))
            {
                this.ModelState.AddModelError("ConfirmPassword", "Confirm password should match password.");
                return this.View(registerViewModel);
            }
            var user = this.modelMapper.ToModel(registerViewModel);
            this.customerService.Update(id, user);

            return this.RedirectToAction("Logout", "Account");
        }
        [MyAuthorisationAttribute(Role = "Employee")]
        public IActionResult Details(int id)
        {
            try
            {
                var customer = this.customerService.GetById(id);
                var customerViewModel = this.modelMapper.ToViewModel(customer);
                customerViewModel.CustomerParcels = GetParcels(id);
                return this.View(customerViewModel);
            }
            catch (EntityNotFoundException)
            {
                return this.View("Error");
            }
        }
        private IActionResult EditView(CustomerViewModel beerViewModel)
        {
            beerViewModel.Cities = this.GetCities();
            beerViewModel.Countries = this.GetCountries();
            return this.View(beerViewModel);
        }
        private List<ParcelDto> GetParcels(int customerId)
        {
            var incomingparcels = this.customerService.GetIncomingParcels(customerId).ToList();
            var pastparcels = this.customerService.GetPastParcels(customerId).ToList();

            return incomingparcels.Concat(pastparcels).ToList();
        }

        private SelectList GetCities()
        {
            var cities = this.cityService.GetAll().ToList();
            return new SelectList(cities, "CityId", "City");
        }
        private SelectList GetCountries()
        {
            var countries = this.countryService.GetAll().ToList();
            return new SelectList(countries, "CountryId", "CountryName");
        }
        private IActionResult Error()
        {
            return this.View("error");
        }
        [Route("customer/parcels/incoming")]
        [MyAuthorisationAttribute(Role = "Customer")]
        public IActionResult IncomingParcels()
        {
            var customerId = int.Parse(this.HttpContext.Session.GetString("UserId"));
            var customerParcels = customerService.GetIncomingParcels(customerId);
            ViewData["MethodName"] = "Incoming";
            return View("Parcels", customerParcels);
        }
        [Route("customer/parcels/past")]
        [MyAuthorisationAttribute(Role = "Customer")]
        public IActionResult PastParcels()
        {
            var customerId = int.Parse(this.HttpContext.Session.GetString("UserId"));
            ViewData["MethodName"] = "Past";
            var customerParcels = customerService.GetPastParcels(customerId);
            return View("Parcels", customerParcels);
        }
        [MyAuthorisationAttribute(Role = "Customer")]
        public IActionResult ChangeDeliveryStatus(int parcelId)
        {
            var customerId = int.Parse(this.HttpContext.Session.GetString("UserId"));
            var change = customerService.ChangeDeliveryStatus(customerId, parcelId);
            var customerParcels = customerService.GetPastParcels(customerId);
            ViewData["MethodName"] = "Incoming";
            return this.RedirectToAction("IncomingParcels");
        }
        [MyAuthorisationAttribute(Role = "Employee")]
        public IActionResult id(int id)
        {
            var customer = customerService.GetById(id);
            return View("Index", customer);
        }
    }
}
