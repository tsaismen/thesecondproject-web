﻿using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using DeliverIT.Web.Helpers;
using DeliverIT.Web.Helpers.HelpersContracts;
using DeliverIT.Web.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;

namespace DeliverIT.Web.Controllers
{
    public class AdminController : Controller
    {
        private readonly IWarehouseService warehouseService;
        private readonly IEmployeeService employeeService;
        private readonly ICustomerService customerService;
        private readonly IModelMapper modelMapper;
        private readonly IAuthHelper authHelper;
        private readonly ICityService cityService;
        private readonly ICountryService countryService;

        public AdminController(IWarehouseService warehouseService
            , IEmployeeService employeeService
            , ICustomerService customerService
            , IModelMapper modelMapper
            , IAuthHelper authHelper
            , ICityService cityService
            , ICountryService countryService)
        {
            this.warehouseService = warehouseService;
            this.employeeService = employeeService;
            this.customerService = customerService;
            this.modelMapper = modelMapper;
            this.authHelper = authHelper;
            this.cityService = cityService;
            this.countryService = countryService;
        }


        [HttpGet]
        [MyAuthorisation(Role = "Employee")]
        public IActionResult Index()
        {
            ViewData["EmployeeName"] = this.HttpContext.Session.GetString("CurrentUser");
            ViewData["UserId"] = int.Parse(this.HttpContext.Session.GetString("UserId"));
            var employeeId = this.HttpContext.Session.GetString("UserId");
            var allShipments = this.warehouseService.GetIncomingShipments(int.Parse(employeeId));
            return this.View(allShipments);
        }
        [MyAuthorisation(Role = "Employee")]
        public IActionResult Customers()
        {
            var customers = customerService.GetAll();
            return View(customers);
        }
        [MyAuthorisation(Role = "Employee")]
        public IActionResult Employees()
        {
            var employees = employeeService.GetAll();
            return View(employees);

        }
        [HttpPost]
        [MyAuthorisation(Role = "Employee")]
        public IActionResult SearchCustomer(string searchword)
        {
            try
            {
                var customers = this.customerService.SearchAllFields(searchword);
                return this.View("Customers", customers);
            }
            catch (InvalidInputDataException)
            {
                return this.View();
            }

        }


        [HttpPost]
        [MyAuthorisation(Role = "Employee")]
        public IActionResult UpcomingShipment(int warehouseId)
        {
            try
            {
                var upcomingShipment = this.warehouseService.GetNextOrder(warehouseId);
                return this.View("../Shipment/UpcomingShipment", upcomingShipment);
            }
            catch (EntityNotFoundException)
            {
                return this.RedirectToAction(nameof(this.Index));
            }

        }


        [MyAuthorisationAttribute(Role = "Employee")]
        public IActionResult Edit(int id)
        {
            try
            {
                var employee = this.employeeService.GetById(id);
                var customerViewModel = this.modelMapper.ToViewModel(employee);

                return this.EmployeeView(customerViewModel);
            }
            catch (EntityNotFoundException)
            {
                return this.View("Error");
            }
        }
        [MyAuthorisationAttribute(Role = "Employee")]
        [HttpPost]
        public IActionResult Edit(int id, [Bind("FirstName, LastName, Email, Password, Street,CityId,CountryId,ConfirmPassword,WarehouseId")] EmployeeViewModel customerViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(customerViewModel);
            }

            if (!customerViewModel.Password.Equals(customerViewModel.ConfirmPassword))
            {
                this.ModelState.AddModelError("ConfirmPassword", "Confirm password should match password.");
                return this.View(customerViewModel);
            }
            var user = this.modelMapper.ToModel(customerViewModel);
            this.employeeService.Update(id, user);

            return this.RedirectToAction("Logout", "Account");
        }

        [MyAuthorisation(Role = "Employee")]
        public IActionResult Create()
        {
            var newEmployeeView = new EmployeeViewModel();
            return this.EmployeeView(newEmployeeView);

        }

        [HttpPost]
        [MyAuthorisation(Role = "Employee")]
        public IActionResult Create([Bind("FirstName,LastName,Email,Street,CityId,CountryId,WarehouseId,Password,ConfirmPassword")] EmployeeViewModel employeeView)
        {
            if (!this.ModelState.IsValid)
            {
                return this.EmployeeView(employeeView);
            }

            if (this.authHelper.Exist(employeeView.Email))
            {
                this.ModelState.AddModelError("Email", "User with same email already exists.");
                return this.EmployeeView(employeeView);
            }

            if (!employeeView.Password.Equals(employeeView.ConfirmPassword))
            {
                this.ModelState.AddModelError("ConfirmPassword", "Confirm password should match password.");
                return this.EmployeeView(employeeView);
            }

            try
            {
                var employee = modelMapper.ToModel(employeeView);
                this.employeeService.Create(employee);
                return this.RedirectToAction(nameof(this.Index));
            }
            catch (DuplicateEntityException)
            {
                return View("Error");
            }
        }

        private IActionResult EmployeeView(EmployeeViewModel employeeView)
        {
            employeeView.Warehouses = GetWarehouses();
            employeeView.Cities = GetCities();
            employeeView.Countries = GetCountries();
            return this.View(employeeView);
        }

        private SelectList GetWarehouses()
        {
            var warehouses = this.warehouseService.GetAll();
            return new SelectList(warehouses, "WarehouseId", "Address");
        }
        private SelectList GetCities()
        {
            var cities = this.cityService.GetAll().ToList();
            return new SelectList(cities, "CityId", "City");
        }
        private SelectList GetCountries()
        {
            var countries = this.countryService.GetAll().ToList();
            return new SelectList(countries, "CountryId", "CountryName");
        }


    }
}
