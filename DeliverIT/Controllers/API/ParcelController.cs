﻿using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using DeliverIT.Web.Helpers.HelpersContracts;
using Microsoft.AspNetCore.Mvc;

namespace DeliverIT.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParcelController : ControllerBase
    {

        private readonly IParcelService parcellService;
        private readonly IModelMapper modelMapper;
        private readonly IAuthHelper authHelper;
        public ParcelController(IParcelService parcellService, IModelMapper modelMapper, IAuthHelper authHelper)
        {
            this.parcellService = parcellService;
            this.modelMapper = modelMapper;
            this.authHelper = authHelper;
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id, [FromHeader] string authentication)
        {
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            try
            {
                var parcel = parcellService.Get(id);
                var parcelDto = new ParcelDto(parcel);
                return this.Ok(parcelDto);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }
        [HttpGet("all")]
        public IActionResult GetAll([FromHeader] string authentication)
        {
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            var parcels = parcellService.GetAll();
            return this.Ok(parcels);
        }
        //FILTER
        [HttpGet("filter")]
        public IActionResult FilterBy([FromQuery] int? customerid, string category, int? warehouseid, double? weight, [FromHeader] string authentication)
        {
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            if (customerid == null && category == null && weight == null && warehouseid == null)
            {
                return this.BadRequest();
            }

            var parcels = parcellService.Filter(customerid, category, warehouseid, weight);
            return this.Ok(parcels);

        }

        //SORTBY
        [HttpGet("sort")]
        public IActionResult SortBy([FromQuery] string weight, string arrival, [FromHeader] string authentication)
        {
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            try
            {
                var parcels = parcellService.SortBy(weight, arrival);
                return this.Ok(parcels);
            }
            catch (InvalidInputDataException)
            {
                return this.BadRequest();
            }
        }

        [HttpPost("")]
        public IActionResult Post([FromBody] ParcelWeb parcelWebModel, [FromHeader] string authentication)
        {
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            try
            {
                var parcel = this.modelMapper.ToModel(parcelWebModel);
                var result = this.parcellService.Create(parcel);

                return this.Created("post", result);
            }
            catch (DuplicateEntityException e)
            {
                return this.Conflict(e.Message);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ParcelWeb parcelWebModel, [FromHeader] string authentication)
        {
            if (parcelWebModel == null)
            {
                return this.BadRequest();
            }
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            try
            {
                var parcel = this.modelMapper.ToModel(parcelWebModel);
                var result = parcellService.Update(id, parcel);
                return this.Ok(result);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id, [FromHeader] string authentication)
        {
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            try
            {
                this.parcellService.Delete(id);
                return this.NoContent();
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }
    }
}

