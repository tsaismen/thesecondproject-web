﻿using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using DeliverIT.Web.Helpers.HelpersContracts;
using Microsoft.AspNetCore.Mvc;

namespace DeliverIT.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CityController : ControllerBase
    {
        private readonly ICityService citiesService;
        private readonly IModelMapper modelMapper;

        public CityController(ICityService citiesService, IModelMapper modelMapper)
        {
            this.citiesService = citiesService;
            this.modelMapper = modelMapper;
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var city = citiesService.Get(id);
                var cityDto = new CityDto(city);
                return this.Ok(city);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }
        [HttpGet("all")]
        public IActionResult GetAll()
        {
            var cities = citiesService.GetAll();
            return this.Ok(cities);
        }
    }
}
