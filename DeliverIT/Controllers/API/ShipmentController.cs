﻿using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using DeliverIT.Web.Helpers.HelpersContracts;
using Microsoft.AspNetCore.Mvc;

namespace DeliverIT.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShipmentController : ControllerBase
    {

        private readonly IShipmentService shipmentService;
        private readonly IModelMapper modelMapper;
        private readonly IAuthHelper authHelper;

        public ShipmentController(IShipmentService shipmentService, IModelMapper modelMapper, IAuthHelper authHelper)
        {
            this.shipmentService = shipmentService;
            this.modelMapper = modelMapper;
            this.authHelper = authHelper;
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id, [FromHeader] string authentication)
        {
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            try
            {
                var shipment = shipmentService.GetById(id);
                var shipmentDto = new ShipmentDto(shipment);
                return this.Ok(shipmentDto);
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }
        [HttpGet("all")]
        public IActionResult GetAll([FromHeader] string authentication)
        {
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            var shipments = shipmentService.GetAll();
            return this.Ok(shipments);
        }

        //filter by warehouse
        //TODO: Routes for shipment filters  ---shipment/warehouse?id=2
        [HttpGet("warehouse/{warehouseId}")]
        public IActionResult GetByWarehouse(int? warehouseId, [FromHeader] string authentication)
        {
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            var shipment = shipmentService.FilterWarehouse(warehouseId);
            return Ok(shipment);
        }

        //filter by customer
        [HttpGet("customer/{customerId}")]
        public IActionResult GetByCustomer(int? customerId, [FromHeader] string authentication)
        {
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            var shipments = shipmentService.GetByCustomer(customerId);
            return this.Ok(shipments);
        }

        [HttpPost("")]
        public IActionResult Post([FromBody] ShipmentWeb shipmentWebModel, [FromHeader] string authentication)
        {
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            try
            {
                var shipment = this.modelMapper.ToModel(shipmentWebModel);
                var result = this.shipmentService.Create(shipment);

                return this.Created("post", result);
            }
            catch (DuplicateEntityException e)
            {
                return this.Conflict(e.Message);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ShipmentWeb shipmentWebModel, [FromHeader] string authentication)
        {
            if (shipmentWebModel == null)
            {
                return this.BadRequest();
            }
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            try
            {
                var shipment = this.modelMapper.ToModel(shipmentWebModel);
                var result = shipmentService.Update(id, shipment);
                return this.Ok(result);
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id, [FromHeader] string authentication)
        {
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            try
            {
                this.shipmentService.Delete(id);
                return this.NoContent();
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }
    }
}
