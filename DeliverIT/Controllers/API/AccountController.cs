﻿using DeliverIT.Models;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using DeliverIT.Web.Helpers.HelpersContracts;
using Microsoft.AspNetCore.Mvc;

namespace DeliverIT.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly ICustomerService customerService;
        private readonly IModelMapper modelMapper;

        public AccountController(ICustomerService customerService, IModelMapper modelMapper)
        {
            this.customerService = customerService;
            this.modelMapper = modelMapper;
        }
        [HttpPost("Register")]
        public IActionResult RegisterApi([FromBody] CustomerWeb customerWebModel)
        {
            try
            {
                var customer = this.modelMapper.ToModel(customerWebModel);
                var result = this.customerService.Create(customer);
                return this.Ok(result);
            }
            catch (DuplicateEntityException e)
            {
                return this.Conflict(e.Message);
            }
        }
    }
}
