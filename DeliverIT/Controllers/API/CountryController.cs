﻿using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using DeliverIT.Web.Helpers.HelpersContracts;
using Microsoft.AspNetCore.Mvc;

namespace DeliverIT.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountryController : ControllerBase
    {
        private readonly ICountryService countriesService;
        private readonly IModelMapper modelMapper;

        public CountryController(ICountryService countriesService, IModelMapper modelMapper)
        {
            this.countriesService = countriesService;
            this.modelMapper = modelMapper;
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var country = countriesService.Get(id);
                var countryDto = new CountryDto(country);
                return this.Ok(countryDto);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }
        [HttpGet("all")]
        public IActionResult GetAll()
        {
            var countries = this.countriesService.GetAll();
            return Ok(countries);
        }
    }
}
