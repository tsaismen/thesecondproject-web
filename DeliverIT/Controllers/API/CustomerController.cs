﻿using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using DeliverIT.Web.Helpers.HelpersContracts;
using Microsoft.AspNetCore.Mvc;

namespace DeliverIT.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService customerService;
        private readonly IModelMapper modelMapper;
        private readonly IAuthHelper authHelper;

        public CustomerController(ICustomerService customerService, IModelMapper modelMapper, IAuthHelper authHelper)
        {
            this.customerService = customerService;
            this.modelMapper = modelMapper;
            this.authHelper = authHelper;
        }
        [HttpGet("{id}")]
        public IActionResult Get(int id, [FromHeader] string authentication)
        {
            if (!authHelper.MakeCustomerOrEmployeeValidation(id, authentication))
                return this.Unauthorized();
            try
            {
                var customer = customerService.GetById(id);
                var customerDto = new CustomerDto(customer);
                return this.Ok(customerDto);
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }
        [HttpGet("all")]
        public IActionResult GetAll([FromHeader] string authentication)
        {
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            var customers = customerService.GetAll();
            return Ok(customers);
        }

        [HttpGet("count")]
        public IActionResult GetCount()
        {
            var count = customerService.GetCount();
            return this.Ok(count);
        }

        //search by email
        [HttpGet("search/email")]
        public IActionResult GetByEmail([FromQuery] string value, [FromHeader] string authentication)
        {
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            var customer = customerService.GetByEmail(value);
            return this.Ok(customer);
        }

        //customer incoming parcells
        [HttpGet("{id}/parcels/incoming")]
        public IActionResult GetIncomingParcels(int? id, [FromHeader] string authentication)
        {
            if (!authHelper.MakeCustomerOrEmployeeValidation(id, authentication))
                return this.Unauthorized();
            var parcelsForCustomer = customerService.GetIncomingParcels(id);
            return this.Ok(parcelsForCustomer);
        }

        //customer incoming parcells
        [HttpGet("{id}/parcels/past")]
        public IActionResult GetPastParcels(int? id, [FromHeader] string authentication)
        {
            if (!authHelper.MakeCustomerOrEmployeeValidation(id, authentication))
                return this.Unauthorized();
            var parcelsForCustomer = customerService.GetPastParcels(id);
            return this.Ok(parcelsForCustomer);
        }
        [HttpPut("{customerId}/parcels/{parcelId}/delivery/change")]
        public IActionResult ChangeDeliveryStatus(int customerId, int parcelId, [FromHeader] string authentication)
        {
            if (!authHelper.MakeCustomerOrEmployeeValidation(customerId, authentication))
                return this.Unauthorized();
            var message = customerService.ChangeDeliveryStatus(customerId, parcelId);
            if (message == "Error")
                return this.BadRequest();
            return this.Ok(message);
        }

        //filter by multiple
        //first and last name
        [HttpGet("search/name")]
        public IActionResult FilterByFirstAndLastName([FromQuery] string firstname, string lastname, [FromHeader] string authentication)
        {
            if (firstname == null && lastname == null)
            {
                return this.BadRequest();
            }
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            var filteredCustomers = customerService.FilterByFirstAndLastName(firstname, lastname);
            return this.Ok(filteredCustomers);
        }
        ////search by name
        //[HttpGet("search/name")]
        //public IActionResult GetByName([FromQuery] string value, [FromHeader] string authentication)
        //{
        //    if (!authHelper.MakeEmployeeValidation(authentication))
        //        return this.Unauthorized();
        //    var customer = customerService.GetByName(value);
        //    return this.Ok(customer);
        //}

        //search all fields
        [HttpGet("search/allfields")]
        public IActionResult SearchAllFields([FromQuery] string searchWord, [FromHeader] string authentication)
        {
            if (searchWord == null)
            {
                return this.BadRequest();
            }
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            var searched = customerService.SearchAllFields(searchWord);
            return this.Ok(searched);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] CustomerWeb customerWebModel, [FromHeader] string authentication)
        {
            if (customerWebModel == null)
            {
                return this.BadRequest();
            }
            if (!authHelper.MakeCustomerOrEmployeeValidation(id, authentication))
                return this.Unauthorized();
            try
            {
                var customer = this.modelMapper.ToModel(customerWebModel);
                var result = customerService.Update(id, customer);
                return this.Ok(result);
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id, [FromHeader] string authentication)
        {
            if (!authHelper.MakeCustomerOrEmployeeValidation(id, authentication))
                return this.Unauthorized();
            try
            {
                this.customerService.Delete(id);
                return this.Ok("User deleted");
            }
            catch (EntityNotFoundException)
            {
                return this.NotFound();
            }
        }
    }
}
