﻿using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using DeliverIT.Web.Helpers.HelpersContracts;
using Microsoft.AspNetCore.Mvc;

namespace DeliverIT.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WarehouseController : ControllerBase
    {
        private readonly IWarehouseService warehouseService;
        private readonly IModelMapper modelMapper;
        private readonly IAuthHelper authHelper;

        public WarehouseController(IWarehouseService warehouseService, IModelMapper modelMapper, IAuthHelper authHelper)
        {
            this.warehouseService = warehouseService;
            this.modelMapper = modelMapper;
            this.authHelper = authHelper;
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var warehouse = warehouseService.Get(id);
                var warehouseDto = new WarehouseDto(warehouse);
                return this.Ok(warehouseDto);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }
        [HttpGet("all")]
        public IActionResult GetAll()
        {
            var warehouses = warehouseService.GetAll();
            return this.Ok(warehouses);
        }

        [HttpGet("{id}/nextshipment")]
        public IActionResult GetNextOrder(int id, [FromHeader] string authentication)
        {
            try
            {
                if (!authHelper.MakeEmployeeValidation(authentication))
                    return this.Unauthorized();
                var shipment = warehouseService.GetNextOrder(id);
                return this.Ok(shipment);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }
        [HttpPost("")]
        public IActionResult Post([FromBody] WarehouseWeb warehouseWebModel, [FromHeader] string authentication)
        {
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            try
            {
                var warehouse = this.modelMapper.ToModel(warehouseWebModel);
                var result = this.warehouseService.Create(warehouse);

                return this.Created("post", result);
            }
            catch (DuplicateEntityException e)
            {
                return this.Conflict(e.Message);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] WarehouseWeb warehouseWebModel, [FromHeader] string authentication)
        {
            if (warehouseWebModel == null)
            {
                return this.BadRequest();
            }
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            try
            {
                var warehouse = this.modelMapper.ToModel(warehouseWebModel);
                var result = warehouseService.Update(id, warehouse);
                return this.Ok(result);
            }
            catch (EntityNotFoundException e)
            {
                return this.NotFound(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id, [FromHeader] string authentication)
        {
            if (!authHelper.MakeEmployeeValidation(authentication))
                return this.Unauthorized();
            try
            {
                this.warehouseService.Delete(id);
                return this.NoContent();
            }
            catch (EntityNotFoundException e)
            {

                return this.NotFound(e.Message);
            }
        }
    }
}
