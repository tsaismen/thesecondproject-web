﻿using DeliverIT.Services.Services.ServicesInterfaces;
using Microsoft.AspNetCore.Mvc;

namespace DeliverIT.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICustomerService customerService;

        public HomeController(ICustomerService customerService)
        {
            this.customerService = customerService;
        }
        public IActionResult Index()
        {
            var userCount = customerService.GetCount();
            this.ViewData["UserCount"] = userCount;
            return View();
        }
        public IActionResult About()
        {
            return View();
        }

        public IActionResult Pricing()
        {
            return View();
        }

    }
}
