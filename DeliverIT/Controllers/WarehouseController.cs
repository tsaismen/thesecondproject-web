﻿using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using DeliverIT.Web.Helpers;
using DeliverIT.Web.Helpers.HelpersContracts;
using DeliverIT.Web.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;
using System.Linq;

namespace DeliverIT.Web.Controllers
{
    public class WarehouseController : Controller
    {
        private readonly IWarehouseService warehouseService;
        private readonly IModelMapper modelMapper;
        private readonly ICityService cityService;
        private readonly ICountryService countryService;

        public WarehouseController(IWarehouseService warehouseService, IModelMapper modelMapper, ICityService cityService, ICountryService countryService)
        {
            this.warehouseService = warehouseService;
            this.modelMapper = modelMapper;
            this.cityService = cityService;
            this.countryService = countryService;
        }
        public IActionResult Index()
        {
            var allWarehouses = warehouseService.GetAll();
            return View(allWarehouses);
        }
        [Route("/warehouse/{id}/nextorder")]
        [MyAuthorisationAttribute(Role = "Employee")]
        public IActionResult Next(int id)
        {
            var nextShipment = warehouseService.GetNextOrder(id);
            return View("../shipment/Details", nextShipment);
        }
        [MyAuthorisationAttribute(Role = "Employee")]
        public IActionResult Create()
        {
            var warehouseViewModel = new WarehouseViewModel();
            return WarehouseView(warehouseViewModel);
        }

        [HttpPost]
        [MyAuthorisationAttribute(Role = "Employee")]
        public IActionResult Create([Bind("Street,CityId,CountryId")] WarehouseViewModel warehouseViewModel)
        {
            if (!ModelState.IsValid)
            {
                return WarehouseView(warehouseViewModel);
            }
            try
            {
                var warehouse = modelMapper.ToModel(warehouseViewModel);
                warehouseService.Create(warehouse);
                return this.RedirectToAction(nameof(this.Index));
            }
            catch (DuplicateEntityException)
            {
                return View("Error");
            }
        }
        [MyAuthorisationAttribute(Role = "Employee")]
        public IActionResult Edit(int id)
        {
            try
            {
                var warehouseToUpdate = warehouseService.Get(id);
                var warehouseViewModel = modelMapper.ToViewModel(warehouseToUpdate);
                return WarehouseView(warehouseViewModel);
            }
            catch (EntityNotFoundException)
            {
                return View("Error");
            }
        }

        [HttpPost]
        [MyAuthorisationAttribute(Role = "Employee")]
        public IActionResult Edit(int id, [Bind("Street,CityId,CountryId")] WarehouseViewModel warehouseViewModel)
        {
            if (!ModelState.IsValid)
            {
                return WarehouseView(warehouseViewModel);
            }
            try
            {
                var warehouse = modelMapper.ToModel(warehouseViewModel);
                warehouseService.Update(id, warehouse);
                return this.RedirectToAction(nameof(this.Index));
            }
            catch (DuplicateEntityException)
            {
                return View("Error");
            }
        }
        [MyAuthorisation(Role = "Employee")]
        public IActionResult DeleteConfirmed(int id)
        {
            try
            {
                this.warehouseService.Delete(id);
            }
            catch (EntityNotFoundException)
            {
                return this.View("Error");
            }

            return this.RedirectToAction(nameof(this.Index));
        }
        private IActionResult WarehouseView(WarehouseViewModel warehouseViewModel)
        {
            warehouseViewModel.Cities = GetCities();
            warehouseViewModel.Countries = GetCountries();
            return View(warehouseViewModel);
        }
        private SelectList GetCities()
        {
            var cities = this.cityService.GetAll().ToList();
            return new SelectList(cities, "CityId", "City");
        }
        private SelectList GetCountries()
        {
            var countries = this.countryService.GetAll().ToList();
            return new SelectList(countries, "CountryId", "CountryName");
        }
    }
}
