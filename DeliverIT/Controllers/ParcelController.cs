﻿using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using DeliverIT.Web.Helpers;
using DeliverIT.Web.Helpers.HelpersContracts;
using DeliverIT.Web.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;

namespace DeliverIT.Web.Controllers
{
    public class ParcelController : Controller
    {
        private readonly IParcelService parcelService;
        private readonly IModelMapper modelMapper;
        private readonly ICustomerService customerService;
        private readonly IShipmentService shipmentService;
        private readonly IWarehouseService warehouseService;

        public ParcelController(IParcelService parcelService
                               , IModelMapper modelMapper
                               , ICustomerService customerService
                               , IShipmentService shipmentService
                               , IWarehouseService warehouseService)
        {
            this.parcelService = parcelService;
            this.modelMapper = modelMapper;
            this.customerService = customerService;
            this.shipmentService = shipmentService;
            this.warehouseService = warehouseService;
        }
        [HttpGet]
        [MyAuthorisation(Role = "Employee")]
        public IActionResult Index()
        {
            var allParcels = this.parcelService.GetAll();
            return View(allParcels);
        }
        [MyAuthorisation(Role = "Employee")]
        public IActionResult Create()
        {
            var newParcelViewModel = new ParcelViewModel();
            return this.ParcelView(newParcelViewModel);

        }
        [HttpPost]
        [MyAuthorisation(Role = "Employee")]
        public IActionResult Create([Bind("WarehouseId,Weight,CategoryId,ShipmentId,CustomerId,HomeDelivery")] ParcelViewModel parcelViewModel)
        {
            if (!ModelState.IsValid)
            {
                return ParcelView(parcelViewModel);
            }
            try
            {
                var parcel = modelMapper.ToModel(parcelViewModel);
                this.parcelService.Create(parcel);
                return this.RedirectToAction(nameof(this.Index));
            }
            catch (DuplicateEntityException)
            {
                return View("Error");
            }
        }
        [MyAuthorisation(Role = "Employee")]
        public IActionResult Edit(int id)
        {
            try
            {
                var parcel = this.parcelService.Get(id);
                var parcelViewModel = this.modelMapper.ToViewModel(parcel);

                return this.ParcelView(parcelViewModel);
            }
            catch (EntityNotFoundException)
            {
                return this.View("Error");
            }
        }
        [HttpPost]
        [MyAuthorisation(Role = "Employee")]
        public IActionResult Edit(int id, [Bind("WarehouseId,Weight,CategoryId,ShipmentId,CustomerId,HomeDelivery")] ParcelViewModel parcelViewModel)
        {
            if (!ModelState.IsValid)
            {
                return ParcelView(parcelViewModel);
            }
            try
            {
                var parcel = modelMapper.ToModel(parcelViewModel);
                this.parcelService.Update(id, parcel);
                return this.RedirectToAction(nameof(this.Index));
            }
            catch (EntityNotFoundException)
            {
                return View("Error");
            }
        }
        [MyAuthorisation(Role = "Employee")]
        public IActionResult DeleteConfirmed(int id)
        {
            try
            {
                this.parcelService.Delete(id);
            }
            catch (EntityNotFoundException)
            {
                return this.View("Error");
            }

            return this.RedirectToAction(nameof(this.Index));
        }
        private IActionResult ParcelView(ParcelViewModel parcelView)
        {
            parcelView.Shipment = GetShipments(parcelView.WarehouseId);
            parcelView.Customer = GetCustomers();
            parcelView.Category = GetCategories();
            parcelView.Warehouse = GetWarehouses();
            return this.View(parcelView);
        }

        //Gets shipments with status preparing in the warehouse that the parcel is in.
        private SelectList GetShipments(int warehouseId)
        {
            var shipments = this.shipmentService.GetAll()
                .Where(x => x.Status == "Preparing" && x.OriginId == warehouseId);
            return new SelectList(shipments, "ShipmentId", "FromTo");
        }
        private SelectList GetCategories()
        {
            var parcels = this.parcelService.GetAllCategories();
            return new SelectList(parcels, "CategoryId", "Name");
        }
        private SelectList GetCustomers()
        {
            var customers = this.customerService.GetAll();
            return new SelectList(customers, "CustomerId", "FirstName", "LastName");
        }
        private SelectList GetWarehouses()
        {
            var warehouses = this.warehouseService.GetAll();
            return new SelectList(warehouses, "WarehouseId", "Address");
        }
    }
}
