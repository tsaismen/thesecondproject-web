﻿using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using DeliverIT.Web.Helpers.HelpersContracts;
using DeliverIT.Web.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;

namespace DeliverIT.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAuthHelper authHelper;
        private readonly ICustomerService userService;
        private readonly IModelMapper modelMapper;
        private readonly ICountryService countryService;
        private readonly ICityService cityService;

        public AccountController(IAuthHelper authHelper, ICustomerService userService, IModelMapper modelMapper, ICountryService countryService, ICityService cityService)
        {
            this.authHelper = authHelper;
            this.userService = userService;
            this.modelMapper = modelMapper;
            this.countryService = countryService;
            this.cityService = cityService;
        }

        //GET: /auth/login
        public IActionResult Login()
        {
            var loginViewModel = new LoginViewModel();

            return this.View(loginViewModel);
        }

        //POST: /auth/login
        [HttpPost]
        public IActionResult Login([Bind("Email, Password")] LoginViewModel loginViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(loginViewModel);
            }

            try
            {
                var user = this.authHelper.TryGetUser(loginViewModel.Email, loginViewModel.Password);
                this.HttpContext.Session.SetString("UserId", user.Id.ToString());
                this.HttpContext.Session.SetString("CurrentUser", user.Email);
                this.HttpContext.Session.SetString("UserRole", user.Role);

                return this.RedirectToAction("index", "home");
            }
            catch (AuthenticationException e)
            {
                this.ModelState.AddModelError("Username", e.Message);
                return this.View(loginViewModel);
            }
        }

        //GET: /auth/logout
        public IActionResult Logout()
        {
            this.HttpContext.Session.Remove("CurrentUser");

            return this.RedirectToAction("index", "home");
        }

        //GET: /auth/register
        public IActionResult Register()
        {
            var registerViewModel = new CustomerViewModel();
            return RegisterView(registerViewModel);
        }

        [HttpPost]
        public IActionResult Register([Bind("FirstName, LastName, Email, Password, Street,CityId,CountryId,ConfirmPassword,")] CustomerViewModel registerViewModel)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(registerViewModel);
            }


            if (this.authHelper.Exist(registerViewModel.Email))
            {
                this.ModelState.AddModelError("Email", "User with same email already exists.");
                return this.View(registerViewModel);
            }

            if (!registerViewModel.Password.Equals(registerViewModel.ConfirmPassword))
            {
                this.ModelState.AddModelError("ConfirmPassword", "Confirm password should match password.");
                return this.View(registerViewModel);
            }


            var user = this.modelMapper.ToModel(registerViewModel);
            this.userService.Create(user);

            return this.RedirectToAction(nameof(this.Login));
        }
        private IActionResult RegisterView(CustomerViewModel beerViewModel)
        {
            beerViewModel.Cities = this.GetCities();
            beerViewModel.Countries = this.GetCountries();
            return this.View(beerViewModel);
        }

        private SelectList GetCities()
        {
            var cities = this.cityService.GetAll().ToList();
            return new SelectList(cities, "CityId", "City");
        }
        private SelectList GetCountries()
        {
            var countries = this.countryService.GetAll().ToList();
            return new SelectList(countries, "CountryId", "CountryName");
        }
        private IActionResult Error()
        {
            return this.View("error");
        }
    }
}
