﻿using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using DeliverIT.Web.Helpers;
using DeliverIT.Web.Helpers.HelpersContracts;
using DeliverIT.Web.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Web.Controllers
{
    public class ShipmentController : Controller
    {
        private readonly IShipmentService shipmentService;
        private readonly IModelMapper modelMapper;
        private readonly IWarehouseService warehouseService;
        private readonly IParcelService parcelService;

        public ShipmentController(IShipmentService shipmentService, IModelMapper modelMapper, IWarehouseService warehouseService, IParcelService parcelService)
        {
            this.shipmentService = shipmentService;
            this.modelMapper = modelMapper;
            this.warehouseService = warehouseService;
            this.parcelService = parcelService;
        }
        [MyAuthorisationAttribute(Role = "Employee")]
        public IActionResult Index()
        {
            var shipments = shipmentService.GetAll();
            return View(shipments);
        }
        [MyAuthorisationAttribute(Role = "Employee")]
        public IActionResult Create()
        {
            var shipmentViewModel = new ShipmentViewModel();
            return ShipmentView(0, shipmentViewModel);
        }
        [HttpPost]
        [MyAuthorisationAttribute(Role = "Employee")]
        public IActionResult Create([Bind("OriginId,DestinationId,DepartureDate,ArrivalDate,ShipmentId,Parcels,Status")] ShipmentViewModel shipmentViewModel)
        {
            if (!ModelState.IsValid || shipmentViewModel.OriginId == shipmentViewModel.DestinationId)
            {
                return ShipmentView(0, shipmentViewModel);
            }
            try
            {
                var shipment = modelMapper.ToModel(shipmentViewModel);
                shipmentService.Create(shipment);
                return this.RedirectToAction(nameof(this.Index));
            }
            catch (DuplicateEntityException)
            {
                return View("Error");
            }
        }
        [MyAuthorisationAttribute(Role = "Employee")]
        public IActionResult Edit(int id)
        {
            try
            {
                var shipmentToUpdate = shipmentService.GetById(id);
                var shipmentViewModel = modelMapper.ToViewModel(shipmentToUpdate);
                return ShipmentView(id, shipmentViewModel);
            }
            catch (EntityNotFoundException)
            {
                return View("Error");
            }
        }

        [HttpPost]
        [MyAuthorisationAttribute(Role = "Employee")]
        public IActionResult Edit(int id, [Bind("OriginId,DestinationId,DepartureDate,ArrivalDate,ShipmentId,Parcels,Status")] ShipmentViewModel shipmentViewModel)
        {
            if (!ModelState.IsValid || shipmentViewModel.OriginId == shipmentViewModel.DestinationId)
            {
                return ShipmentView(id, shipmentViewModel);
            }
            try
            {
                shipmentViewModel.ShipmentId = id;
                var shipment = modelMapper.ToModel(shipmentViewModel);
                shipmentService.Update(id, shipment);
                return this.RedirectToAction(nameof(this.Index));
            }
            catch (DuplicateEntityException)
            {
                return View("Error");
            }
        }
        [MyAuthorisation(Role = "Employee")]
        public IActionResult DeleteConfirmed(int id)
        {
            try
            {
                this.shipmentService.Delete(id);
            }
            catch (EntityNotFoundException)
            {
                return this.View("Error");
            }

            return this.RedirectToAction(nameof(this.Index));
        }
        private IActionResult ShipmentView(int id, ShipmentViewModel shipmentViewModel)
        {
            var warehouses = GetWarehouses();
            shipmentViewModel.OriginWarehouse = warehouses;
            shipmentViewModel.DestinationWarehouse = warehouses;
            shipmentViewModel.UnselectedParcels = GetUnselectedParcels(id, shipmentViewModel.OriginId);
            return View(shipmentViewModel);
        }

        //Gets all parcels that are with no shipment
        private List<SelectListItem> GetUnselectedParcels(int id, int originWarehouseId)
        {
            var parcels = this.shipmentService.GetAllOtherParcels(id, originWarehouseId)
                .Select(x => new SelectListItem()
                {
                    Value = x.ParcelId.ToString(),
                    Text = $"ID:' {x.ParcelId} ' , Location :' {x.Warehouse} ' Customer: '{x.CustomerName}' , Category: ' {x.Category} '"
                }).ToList();
            return parcels;
        }
        private SelectList GetWarehouses()
        {
            var warehouses = this.warehouseService.GetAll().ToList();
            return new SelectList(warehouses, "WarehouseId", "Address");
        }
        [MyAuthorisationAttribute(Role = "Employee")]
        public IActionResult ParcelList()
        {
            return this.View();
        }
    }
}
