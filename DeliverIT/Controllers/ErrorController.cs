﻿using Microsoft.AspNetCore.Mvc;

namespace DeliverIT.Web.Controllers
{
    public class ErrorController : Controller
    {
        public IActionResult Index()
        {
            return View("Error");
        }

        public IActionResult PermissionDenied()
        {
            return View();
        }
    }
}
