﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DeliverIT.Models
{
    public class WarehouseWeb
    {
        [StringLength(20, MinimumLength = 4, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string Street { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "CityId must be positive.")]
        public int CityId { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "CountryId must be positive.")]
        public int CountryId { get; set; }
    }
}