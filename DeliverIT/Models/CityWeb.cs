﻿namespace DeliverIT.Models.AddressFolder
{
    public class CityWeb
    {
        public string Name { get; set; }
        public CountryWeb Country { get; set; }
    }
}