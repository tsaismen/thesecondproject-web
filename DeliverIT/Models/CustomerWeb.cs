﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DeliverIT.Models
{
    public class CustomerWeb
    {
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string LastName { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "Not a valid email address.")]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string Street { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "CityId must be positive.")]
        public int CityId { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "CountryId must be positive.")]
        public int CountryId { get; set; }
    }
}