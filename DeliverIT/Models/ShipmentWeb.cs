﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DeliverIT.Models
{
    public class ShipmentWeb
    {
        [Range(1, int.MaxValue, ErrorMessage = "OriginId must be positive.")]
        public int OriginId { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "DestinationId must be positive.")]
        public int DestinationId { get; set; }
        public Status Status { get; set; }
        public int ShipmentId { get; set; }
        [DataType(DataType.Date)]
        public DateTime DepartureDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime ArrivalDate { get; set; }
        public List<string> Parcels { get; set; }
    }
}