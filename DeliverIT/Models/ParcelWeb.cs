﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DeliverIT.Models
{
    public class ParcelWeb
    {
        [Range(1, int.MaxValue, ErrorMessage = "WarehouseId must be positive.")]
        public int WarehouseId { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Weight must be positive.")]
        public double Weight { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "CategoryId must be positive.")]
        public int CategoryId { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "CustomerId must be positive.")]
        public int CustomerId { get; set; }
        public int? ShipmentId { get; set; }
        public bool HomeDelivery { get; set; }
    }
}