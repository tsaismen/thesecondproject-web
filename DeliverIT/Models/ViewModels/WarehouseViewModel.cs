﻿using DeliverIT.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DeliverIT.Web.Models.ViewModels
{
    public class WarehouseViewModel : WarehouseWeb
    {
        public SelectList Cities { get; set; }
        public SelectList Countries { get; set; }
    }
}
