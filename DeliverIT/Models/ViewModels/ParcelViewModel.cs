﻿using DeliverIT.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DeliverIT.Web.Models.ViewModels
{
    public class ParcelViewModel : ParcelWeb
    {
        public SelectList Warehouse { get; set; }
        public SelectList Shipment { get; set; }
        public SelectList Category { get; set; }
        public SelectList Customer { get; set; }
    }
}
