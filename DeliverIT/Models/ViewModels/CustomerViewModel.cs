﻿using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DeliverIT.Web.Models.ViewModels
{
    public class CustomerViewModel : CustomerWeb
    {
        public int CustomerId { get; set; }
        [Required]
        public string ConfirmPassword { get; set; }
        public SelectList Cities { get; set; }
        public SelectList Countries { get; set; }
        public string Address { get; set; }
        public List<ParcelDto> CustomerParcels { get; set; }
    }
}
