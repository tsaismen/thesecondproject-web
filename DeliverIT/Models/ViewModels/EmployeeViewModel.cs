﻿using DeliverIT.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace DeliverIT.Web.Models.ViewModels
{
    public class EmployeeViewModel : EmployeeWeb
    {
        [Required]
        public string ConfirmPassword { get; set; }
        public SelectList Warehouses { get; set; }
        public SelectList Cities { get; set; }
        public SelectList Countries { get; set; }
    }
}
