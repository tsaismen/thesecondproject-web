﻿using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace DeliverIT.Web.Models.ViewModels
{
    public class ShipmentViewModel : ShipmentWeb
    {
        //shipments parcels view
        public ShipmentDto Upcoming { get; }
        public List<SelectListItem> ShipmentParcels { get; set; }
        public List<SelectListItem> UnselectedParcels { get; set; }
        public SelectList OriginWarehouse { get; set; }
        public SelectList DestinationWarehouse { get; set; }
    }
}
