﻿using System.ComponentModel.DataAnnotations;


namespace DeliverIT.Models
{
    public class CategoryWeb
    {
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public string Name { get; set; }
    }
}