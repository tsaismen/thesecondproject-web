﻿using DeliverIT.Models.AddressFolder;
using System.ComponentModel.DataAnnotations;



namespace DeliverIT.Models
{
    public class AddressWeb
    {
        public string Street { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "City is mandatory")]
        public City City { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Country is mandatory")]
        public Country Country { get; set; }
    }
}