﻿using DeliverIT.Models;
using DeliverIT.Models.AddressFolder;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIt.Data
{
    public static class ModelBuilderExtension
    {
        public static IEnumerable<Address> Addresses { get; }
        public static IEnumerable<City> Cities { get; }
        public static IEnumerable<Country> Countries { get; }
        public static IEnumerable<Employee> Employees { get; }
        public static IEnumerable<Customer> Customers { get; }
        public static IEnumerable<Warehouse> Warehouses { get; }
        public static IEnumerable<Category> Categories { get; }
        public static ICollection<Parcel> Parcels { get; }
        public static IEnumerable<Shipment> Shipments { get; }
        static ModelBuilderExtension()
        {
            Countries = new List<Country>
            {
                new Country
                {
                    CountryId = 1,
                    Name = "Bulgaria"
                },
                new Country
                {
                    CountryId = 2,
                    Name = "France"
                }
            };
            Cities = new List<City>
            {
                new City
                {
                    CityId=1,
                    Name="Montana",
                    CountryId=1,
                },
                new City
                {
                    CityId=2,
                    Name="Paris",
                    CountryId=2
                }
            };
            Addresses = new List<Address>
            {
                new Address
                {
                    AddressId=1,
                    Street="Baba Tonka 32",
                    CityId=1,
                    CountryId=1
                },
                new Address
                {
                    AddressId=2,
                    Street="Patlajan 32",
                    CityId=1,
                    CountryId=1
                },
                new Address
                {
                    AddressId=3,
                    Street="Baguette 32",
                    CityId=2,
                    CountryId=2
                },
                new Address
                {
                    AddressId=4,
                    Street="Kelesho 32",
                    CityId=2,
                    CountryId=2
                }
            };
            Employees = new List<Employee>
            {
                new Employee
                {
                     EmployeeId=1,
                     FirstName="Georgi",
                     LastName="Ivanov",
                     Email="georgiivanov23@gmail.com",
                     AddressId=1
                },
                new Employee
                {
                    EmployeeId=2,
                    FirstName="Parvan",
                    LastName="Stanislavov",
                    Email="parata@gmail.com",
                    AddressId=2
                }
            };
            Customers = new List<Customer>
            {
                new Customer
                {
                    CustomerId=1,
                    FirstName="Ivailo",
                    LastName="Petrov",
                    Email="ivakamadafaka@gmail.com",
                    AddressId=3
                },
                new Customer
                {
                    CustomerId=2,
                    FirstName="Stanko",
                    LastName="Petrov",
                    Email="stankoteslata@gmail.com",
                    AddressId=4
                }
            };
            Warehouses = new List<Warehouse>
            {
                new Warehouse
                {
                    WarehouseId=1,
                    AddressId=1
                },
                new Warehouse
                {
                    WarehouseId=2,
                    AddressId=2
                }

            };
            Categories = new List<Category>
            {
                new Category
                {
                    CategoryId=1,
                    Name="Electronics"
                },
                new Category
                {
                    CategoryId=2,
                    Name="Food"
                },
                new Category
                {
                    CategoryId=3,
                    Name="Clothes"
                }
            };
            Parcels = new List<Parcel>
            {
            new Parcel
                {
                    ParcelId=1,
                    WarehouseId=1,
                    Weight=4,
                    CategoryId=1,
                    CustomerId=1,
                    HomeDelivery=false,
                    ShipmentId=1
                },
            new Parcel
                {
                    ParcelId=2,
                    WarehouseId=2,
                    Weight=2,
                    CategoryId=2,
                    CustomerId=2,
                    HomeDelivery=true,
                    ShipmentId=1
                },
                new Parcel
                {
                    ParcelId=3,
                    WarehouseId=1,
                    Weight=5,
                    CategoryId=3,
                    CustomerId=1,
                    HomeDelivery=false,
                    ShipmentId=2
                },
                new Parcel
                {
                    ParcelId=4,
                    WarehouseId=2,
                    Weight=6,
                    CategoryId=2,
                    CustomerId=2,
                    HomeDelivery=true,
                    ShipmentId=2
                }
            };
            Shipments = new List<Shipment>
            {
                new Shipment
                {
                    ShipmentId=1,
                    parcels = Parcels.Take(2).ToList(),
                    OriginWarehouseId=1,
                    DestinationWarehouseId=2,
                    DepartureDate=DateTime.Now,
                    ArrivalDate=DateTime.Now.AddDays(1),
                    Status=Status.Preparing
                },
                new Shipment
                {
                    ShipmentId=2,
                    parcels = Parcels.Skip(2).Take(2).ToList(),
                    OriginWarehouseId=2,
                    DestinationWarehouseId=1,
                    DepartureDate=DateTime.Now,
                    ArrivalDate=DateTime.Now.AddDays(3),
                    Status=Status.OnTheWay
                }
            };
        }
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Country>().HasData(Countries);
            modelBuilder.Entity<City>().HasData(Cities);
            modelBuilder.Entity<Address>().HasData(Addresses);
            modelBuilder.Entity<Customer>().HasData(Customers);
            modelBuilder.Entity<Employee>().HasData(Employees);
            modelBuilder.Entity<Warehouse>().HasData(Warehouses);
            modelBuilder.Entity<Category>().HasData(Categories);
            modelBuilder.Entity<Parcel>().HasData(Parcels);
            modelBuilder.Entity<Shipment>().HasData(Shipments);
        }
    }
}
