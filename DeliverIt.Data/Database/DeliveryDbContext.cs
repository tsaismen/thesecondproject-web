﻿using DeliverIT.Models;
using DeliverIT.Models.AddressFolder;
using Microsoft.EntityFrameworkCore;

namespace DeliverIt.Data
{
    public class DeliveryDbContext : DbContext, IDatabase
    {
        public DeliveryDbContext(DbContextOptions<DeliveryDbContext> options)
            : base(options)
        {

        }
        public DbSet<Country> Countries { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Parcel> Parcels { get; set; }
        public DbSet<Shipment> Shipments { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Address>()
               .HasOne(s => s.City)
               .WithMany(x => x.Addresses)
               .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Address>()
               .HasOne(s => s.Country)
               .WithMany(x => x.Addresses)
               .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Employee>()
               .HasOne(p => p.Warehouse)
               .WithMany(x => x.Employees)
               .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Employee>()
               .HasOne(p => p.Address)
               .WithMany(x => x.Employees)
               .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Shipment>()
                .HasOne(p => p.Origin)
                .WithMany(x => x.OriginShipments)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Shipment>()
                .HasOne(p => p.Destination)
                .WithMany(x => x.DestinationShipments)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Parcel>()
                .HasOne(p => p.Warehouse)
                .WithMany()
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Parcel>()
                .HasOne(p => p.Shipment)
                .WithMany(x => x.parcels)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Warehouse>()
                .HasOne(x => x.Address)
                .WithMany(x => x.Warehouses)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Customer>().Property<bool>("isDeleted");
            modelBuilder.Entity<Customer>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Entity<Employee>().Property<bool>("isDeleted");
            modelBuilder.Entity<Employee>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Entity<Warehouse>().Property<bool>("isDeleted");
            modelBuilder.Entity<Warehouse>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Entity<Shipment>().Property<bool>("isDeleted");
            modelBuilder.Entity<Shipment>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Entity<Parcel>().Property<bool>("isDeleted");
            modelBuilder.Entity<Parcel>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);

            modelBuilder.Entity<Address>().Property<bool>("isDeleted");
            modelBuilder.Entity<Address>().HasQueryFilter(m => EF.Property<bool>(m, "isDeleted") == false);
        }
        public override int SaveChanges()
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChanges();
        }
        private void UpdateSoftDeleteStatuses()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                if (entry.GetType().GetProperty("isDeleted") == null)
                {
                    continue;
                }
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.CurrentValues["isDeleted"] = false;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        entry.CurrentValues["isDeleted"] = true;
                        break;
                }
            }
        }
    }
}
