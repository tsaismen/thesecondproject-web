﻿using DeliverIT.Models;
using DeliverIT.Models.AddressFolder;
using Microsoft.EntityFrameworkCore;

namespace DeliverIt.Data
{
    public interface IDatabase
    {
        DbSet<Country> Countries { get; }
        DbSet<City> Cities { get; }
        DbSet<Address> Addresses { get; }
        DbSet<Customer> Customers { get; }
        DbSet<Employee> Employees { get; }
        DbSet<Warehouse> Warehouses { get; }
        DbSet<Category> Categories { get; }
        DbSet<Parcel> Parcels { get; }
        DbSet<Shipment> Shipments { get; }
        public int SaveChanges();
    }
}
