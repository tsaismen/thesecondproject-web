﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace DeliverIT.Models.AddressFolder
{
    public class City
    {
        [Key]
        public int CityId { get; set; }
        public string Name { get; set; }
        [Required]
        [ForeignKey("Country")]
        public int CountryId { get; set; }
        [JsonIgnore]
        public Country Country { get; set; }
        [JsonIgnore]
        public virtual ICollection<Address> Addresses { get; set; }
    }
}
