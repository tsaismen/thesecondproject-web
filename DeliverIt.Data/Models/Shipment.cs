﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace DeliverIT.Models
{
    public class Shipment
    {
        [Key]
        public int ShipmentId { get; set; }
        [JsonIgnore]
        public ICollection<Parcel> parcels { get; set; } = new List<Parcel>();

        [ForeignKey("Warehouse")]
        public int OriginWarehouseId { get; set; }
        [JsonIgnore]
        public Warehouse Origin { get; set; }

        [ForeignKey("Warehouse")]
        public int DestinationWarehouseId { get; set; }
        [JsonIgnore]
        public Warehouse Destination { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ArrivalDate { get; set; }
        public Status Status { get; set; } = Status.Preparing;

        public bool isDeleted { get; set; }
    }
}
