﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace DeliverIT.Models
{
    public class Parcel
    {
        [Key]
        public int ParcelId { get; set; }

        [Required]
        [ForeignKey("Warehouse")]
        public int WarehouseId { get; set; }
        [JsonIgnore]
        public Warehouse Warehouse { get; set; }

        public double Weight { get; set; }

        [Required]
        [ForeignKey("Category")]
        public int CategoryId { get; set; }
        [JsonIgnore]
        public Category Category { get; set; }

        [Required]
        [ForeignKey("Customer")]
        public int CustomerId { get; set; }
        [JsonIgnore]
        public Customer Customer { get; set; }

        public bool HomeDelivery { get; set; }

        [ForeignKey("Shipment")]
        public int? ShipmentId { get; set; }
        [JsonIgnore]
        public Shipment Shipment { get; set; }
        public bool isDeleted { get; set; }
    }
}
