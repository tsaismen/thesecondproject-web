﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace DeliverIT.Models
{
    public class Customer
    {
        [Key]
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        [Required]
        [ForeignKey("Address")]
        public int AddressId { get; set; }
        [JsonIgnore]
        public Address Address { get; set; }
        [JsonIgnore]
        public virtual ICollection<Parcel> Parcels { get; set; }
        public bool isDeleted { get; set; }
    }
}
