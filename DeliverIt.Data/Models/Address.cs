﻿using DeliverIT.Models.AddressFolder;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace DeliverIT.Models
{
    public class Address
    {
        [Key]
        public int AddressId { get; set; }

        public string Street { get; set; }

        [Required]
        [ForeignKey("City")]
        public int CityId { get; set; }
        [JsonIgnore]
        public City City { get; set; }

        [Required]
        [ForeignKey("Country")]
        public int CountryId { get; set; }
        [JsonIgnore]
        public Country Country { get; set; }

        [JsonIgnore]
        public virtual ICollection<Customer> Customers { get; set; }
        [JsonIgnore]
        public virtual ICollection<Employee> Employees { get; set; }
        [JsonIgnore]
        public virtual ICollection<Warehouse> Warehouses { get; set; }

        public bool isDeleted { get; set; }

        public override string ToString()
        {
            return $"{this.Street}, {this.City.Name}, {this.Country.Name}";
        }
    }
}
