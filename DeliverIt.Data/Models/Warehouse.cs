﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace DeliverIT.Models
{
    public class Warehouse
    {
        [Key]
        public int WarehouseId { get; set; }

        [Required]
        [ForeignKey("Address")]
        public int AddressId { get; set; }
        [JsonIgnore]
        public Address Address { get; set; }
        [JsonIgnore]
        public virtual ICollection<Shipment> OriginShipments { get; set; }
        [JsonIgnore]
        public virtual ICollection<Shipment> DestinationShipments { get; set; }
        [JsonIgnore]
        public virtual ICollection<Employee> Employees { get; set; }
        [JsonIgnore]
        public virtual ICollection<Parcel> Parcels { get; set; }
        public bool isDeleted { get; set; }

    }
}
