﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace DeliverIT.Models
{
    public class Employee
    {
        [Key]
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }


        [ForeignKey("Address")]
        public int? AddressId { get; set; }
        [JsonIgnore]
        public Address Address { get; set; }

        [ForeignKey("Warehouse")]
        public int WarehouseId { get; set; }
        [JsonIgnore]
        public Warehouse Warehouse { get; set; }
        public bool isDeleted { get; set; }
    }
}
