﻿namespace DeliverIT.Models
{
    public enum Status
    {
        Preparing,
        OnTheWay,
        Completed
    }
}
