﻿using DeliverIT.Models.AddressFolder;

namespace DeliverIT.Services.DTOs
{
    public class CityDto
    {
        public CityDto()
        {

        }

        public CityDto(City city)
        {
            this.CityId = city.CityId;
            this.City = city.Name;
            this.Country = city.Country.Name;
        }
        public int CityId { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
