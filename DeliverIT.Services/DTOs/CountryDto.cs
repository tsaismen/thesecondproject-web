﻿using DeliverIT.Models.AddressFolder;

namespace DeliverIT.Services.DTOs
{
    public class CountryDto
    {
        public CountryDto()
        {

        }
        public CountryDto(Country country)
        {
            this.CountryId = country.CountryId;
            this.CountryName = country.Name;
        }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
    }
}
