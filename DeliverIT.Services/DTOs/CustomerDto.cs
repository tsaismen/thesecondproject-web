﻿using DeliverIT.Models;

namespace DeliverIT.Services.DTOs
{
    public class CustomerDto
    {
        public CustomerDto()
        {

        }
        public CustomerDto(Customer customer)
        {
            this.CustomerId = customer.CustomerId;
            this.FirstName = customer.FirstName;
            this.LastName = customer.LastName;
            this.Email = customer.Email;
            this.Address = customer.Address.ToString();
        }
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
    }
}
