﻿using DeliverIT.Models;

namespace DeliverIT.Services.DTOs
{
    public class EmployeeDto
    {

        public EmployeeDto()
        {

        }
        public EmployeeDto(Employee employee)
        {
            this.EmployeeId = employee.EmployeeId;
            this.FirstName = employee.FirstName;
            this.LastName = employee.LastName;
            this.Email = employee.Email;
            this.Address = employee.Address == null ? "-" : employee.Address.ToString();
        }
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
    }
}
