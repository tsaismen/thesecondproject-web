﻿using DeliverIT.Models;

namespace DeliverIT.Services.DTOs
{
    public class WarehouseDto
    {
        public WarehouseDto()
        {

        }
        public WarehouseDto(Warehouse warehouse)
        {
            this.WarehouseId = warehouse.WarehouseId;
            this.Address = warehouse.Address.ToString();
        }
        public int WarehouseId { get; set; }
        public string Address { get; set; }
    }
}
