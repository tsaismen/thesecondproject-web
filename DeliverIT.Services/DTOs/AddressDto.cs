﻿using DeliverIT.Models;

namespace DeliverIT.Services.DTOs
{
    public class AddressDto
    {
        public AddressDto()
        {

        }

        public AddressDto(Address address)
        {
            this.AddressId = address.AddressId;
            this.Street = address.Street;
            this.City = address.City.Name;
            this.Country = address.Country.Name;
        }
        public int AddressId { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}