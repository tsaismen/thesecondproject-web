﻿using DeliverIT.Models;

namespace DeliverIT.Services.DTOs
{
    public class ShipmentDto
    {
        public ShipmentDto()
        {

        }

        public ShipmentDto(Shipment shipment)
        {
            this.ShipmentId = shipment.ShipmentId;
            this.Origin = shipment.Origin.Address.ToString();
            this.OriginId = shipment.OriginWarehouseId;
            this.Destination = shipment.Destination.Address.ToString();
            this.DestinationId = shipment.DestinationWarehouseId;
            this.DepartureDate = shipment.DepartureDate.ToShortDateString();
            this.ArrivalDate = shipment.ArrivalDate.ToShortDateString();
            this.Status = shipment.Status.ToString();
            this.FromTo = $"ShipmentID : '{ShipmentId}' Departure from:' {Origin} ' Ships to:'{Destination}'";
        }

        public int ShipmentId { get; set; }
        public string Origin { get; set; }
        public int OriginId { get; set; }
        public string Destination { get; set; }
        public int DestinationId { get; set; }
        public string DepartureDate { get; set; }
        public string ArrivalDate { get; set; }
        public string Status { get; set; }
        public string FromTo { get; set; }
    }
}
