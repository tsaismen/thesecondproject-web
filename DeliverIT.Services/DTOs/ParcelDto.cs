﻿using DeliverIT.Models;

namespace DeliverIT.Services.DTOs
{
    public class ParcelDto
    {
        public ParcelDto()
        {

        }

        public ParcelDto(Parcel parcel)
        {
            this.ParcelId = parcel.ParcelId;
            this.Warehouse = $"{parcel.Warehouse.Address.City.Name},{parcel.Warehouse.Address.Country.Name}";
            this.CustomerName = parcel.Customer.FirstName + " " + parcel.Customer.LastName;
            this.Category = parcel.Category.Name;
            this.Weight = parcel.Weight;
            this.ShipmentID = parcel.Shipment == null ? "-" : parcel.ShipmentId.ToString();
            this.Status = parcel.Shipment == null ? "-" : parcel.Shipment.Status.ToString();
            this.ArrivalDate = parcel.Shipment == null ? "-" : parcel.Shipment.ArrivalDate.ToShortDateString();
            this.Delivery = parcel.HomeDelivery == true ? "Home Delivery" : "Pickup";
        }
        public int ParcelId { get; set; }
        public string Warehouse { get; set; }
        public string CustomerName { get; set; }
        public string Category { get; set; }
        public double Weight { get; set; }
        public string ShipmentID { get; set; }
        public string Status { get; set; }
        public string ArrivalDate { get; set; }
        public string Delivery { get; set; }
    }
}
