﻿using System;

namespace DeliverIT.Services.Exceptions
{
    public class DuplicateEntityException : ApplicationException
    {
        public DuplicateEntityException()
        {
        }

        public DuplicateEntityException(string message)
            : base(message)
        {
        }
    }
}
