﻿using System;

namespace DeliverIT.Services.Exceptions
{
    public class AuthenticationException : ApplicationException
    {
        public AuthenticationException()
        {

        }

        public AuthenticationException(string message)
            : base(message)
        {

        }
    }
}
