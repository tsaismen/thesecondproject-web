﻿using System;

namespace DeliverIT.Services.Exceptions
{
    public class InvalidInputDataException : ApplicationException
    {
        public InvalidInputDataException()
        {
        }

        public InvalidInputDataException(string message)
            : base(message)
        {
        }
    }
}
