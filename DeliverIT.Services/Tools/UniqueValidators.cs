﻿using DeliverIt.Data;
using DeliverIT.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace DeliverIT.Services.Tools
{
    public class UniqueValidators
    {
        private readonly IDatabase database;

        public UniqueValidators(IDatabase database)
        {
            this.database = database;
        }
        public Address ValidateAddress(Address incomingAddress)
        {
            if (database.Addresses.Any(x => x.Street.ToLower().Equals(incomingAddress.Street.ToLower())))
            {
                var existingAddress = database.Addresses
                    .Include(x => x.Country)
                    .Include(x => x.City)
                    .FirstOrDefault(x => x.Street.ToLower().Equals(incomingAddress.Street.ToLower()));
                return existingAddress;
            }
            return incomingAddress;
        }
        public bool ValidateEmployeeEmail(string incomingEmail)
        {
            if (database.Employees.Any(x => x.Email.ToLower().Equals(incomingEmail.ToLower())))
            {
                return true;
            }
            return false;
        }
        public bool ValidateCustomerEmail(string incomingEmail)
        {
            if (database.Customers.Any(x => x.Email.ToLower().Equals(incomingEmail.ToLower())))
            {
                return true;
            }
            return false;
        }


    }
}
