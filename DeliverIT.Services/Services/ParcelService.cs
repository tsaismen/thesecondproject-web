﻿using DeliverIt.Data;
using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services.Services
{
    public class ParcelService : IParcelService
    {
        private readonly IDatabase database;
        public ParcelService(IDatabase database)
        {
            this.database = database;
        }

        // Property for easy access to repeating querry
        private IQueryable<Parcel> Querry
        {
            get
            {
                return this.database.Parcels
                .Include(x => x.Warehouse)
                .ThenInclude(x => x.Address)
                .ThenInclude(x => x.Country)
                .Include(x => x.Warehouse)
                .ThenInclude(x => x.Address)
                .ThenInclude(x => x.City)
                .Include(x => x.Shipment)
                .Include(x => x.Category)
                .Include(x => x.Customer);
            }
        }

        //Gets single parcel by id
        public Parcel Get(int? id)
        {
            var parcel = this.Querry
                .FirstOrDefault(x => x.ParcelId == id);
            return parcel ?? throw new EntityNotFoundException();
        }

        //Gets all parcels in database
        public IEnumerable<ParcelDto> GetAll()
        {
            var parcels = this.Querry;
            return ListModelByDTO(parcels);
        }

        //Filters parcels by customer id, category,warehouseid and weight.
        //Works with any number of the parameters
        public IEnumerable<ParcelDto> Filter(int? customerid, string category, int? warehouseid, double? weight)
        {
            var parcels = this.Querry.ToList();

            if (customerid != null)
            {
                parcels = this.database.Parcels.Where(x => x.Customer.CustomerId.Equals(customerid)).ToList();
            }
            if (category != null)
            {
                parcels = parcels.Where(x => x.Category.Name.Equals(category, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }
            if (warehouseid != null)
            {
                parcels = parcels.Where(x => x.Warehouse.WarehouseId.Equals(warehouseid)).ToList();
            }
            if (weight != null)
            {
                parcels = parcels.Where(x => x.Weight.Equals(weight)).ToList();
            }
            return ListModelByDTO(parcels);
        }

        //Sorts parcels by weight and arrival date
        public IEnumerable<ParcelDto> SortBy(string weight, string arrival)
        {
            var parcels = this.database.Parcels
                .Include(x => x.Shipment)
                .Include(x => x.Warehouse)
                .ThenInclude(x => x.Address)
                .ThenInclude(x => x.City)
                .Include(x => x.Warehouse)
                .ThenInclude(x => x.Address)
                .ThenInclude(x => x.Country)
                .AsEnumerable();

            if (weight == null && arrival == null)
            {
                throw new InvalidInputDataException();
            }
            if (weight != null && arrival != null)
            {
                if (weight != "asc" && weight != "desc" && arrival != "asc" && arrival != "desc")
                {
                    throw new InvalidInputDataException();
                }
                if (arrival == "asc")
                {
                    parcels = parcels.OrderBy(x => x.Shipment.ArrivalDate);
                }
                else
                {
                    parcels = parcels.OrderByDescending(x => x.Shipment.ArrivalDate);
                }
                if (weight == "asc")
                {
                    parcels = parcels.OrderBy(x => x.Weight);
                }
                else
                {
                    parcels = parcels.OrderByDescending(x => x.Weight);
                }
            }
            else if (arrival != null)
            {
                if (arrival != "asc" && arrival != "desc")
                {
                    throw new InvalidInputDataException();
                }
                if (arrival == "asc")
                {
                    parcels = parcels.OrderBy(x => x.Shipment.ArrivalDate);
                }
                else
                {
                    parcels = parcels.OrderByDescending(x => x.Shipment.ArrivalDate);
                }
            }
            else if (weight != null)
            {
                if (weight != "asc" && weight != "desc")
                {
                    throw new InvalidInputDataException();
                }
                if (weight == "asc")
                {
                    parcels = parcels.OrderBy(x => x.Weight);
                }
                else
                {
                    parcels = parcels.OrderByDescending(x => x.Weight);
                }
            }
            return ListModelByDTO(parcels);
        }

        //Creates parcel
        public Parcel Create(Parcel parcel)
        {
            if (this.database.Parcels
                .Any(x => x.Equals(parcel)))
            {
                throw new DuplicateEntityException($"Parcel {parcel} is already in shipment exists.");
            }

            this.database.Parcels.Add(parcel);
            this.database.SaveChanges();

            return parcel;
        }

        //Updates existing parcel by id
        public Parcel Update(int id, Parcel parcel)
        {
            var parcelToUpdate = this.Querry
                .FirstOrDefault(x => x.ParcelId == id);
            if (parcelToUpdate.Shipment == null || parcelToUpdate.Shipment.Status == Status.Preparing)
            {
                parcelToUpdate.ShipmentId = parcel.ShipmentId;
            }
            parcelToUpdate.WarehouseId = parcel.WarehouseId;
            parcelToUpdate.Weight = parcel.Weight;
            parcelToUpdate.CategoryId = parcel.CategoryId;
            parcelToUpdate.CustomerId = parcel.CustomerId;
            parcelToUpdate.HomeDelivery = parcel.HomeDelivery;
            this.database.SaveChanges();

            return parcelToUpdate;
        }

        //Deletes existing parcel
        public void Delete(int id)
        {
            var parcel = this.database.Parcels
                .FirstOrDefault(x => x.ParcelId == id);

            if (parcel == null)
            {
                throw new EntityNotFoundException();
            }
            parcel.isDeleted = true;
            this.database.SaveChanges();
        }

        //Accepts list of Parcel models and transforms them in parcelDto model.
        private IEnumerable<ParcelDto> ListModelByDTO(IEnumerable<Parcel> listofparcels)
        {
            var parcelsDTOs = new List<ParcelDto>();
            foreach (var parcel in listofparcels)
            {
                var dto = new ParcelDto(parcel);
                parcelsDTOs.Add(dto);
            }
            return parcelsDTOs;
        }

        //Gets all existing categories for parcels
        public IEnumerable<Category> GetAllCategories()
        {
            var categories = this.database.Categories.ToList();
            return categories;
        }
    }
}
