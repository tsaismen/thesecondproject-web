﻿using DeliverIt.Data;
using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services.Services
{
    public class ShipmentService : IShipmentService
    {

        private readonly IDatabase database;
        private readonly IParcelService parcellService;

        public ShipmentService(IDatabase database, IParcelService parcellService)
        {
            this.database = database;
            this.parcellService = parcellService;
        }

        // Property for easy access to repeating querry
        private IQueryable<Shipment> Querry
        {
            get
            {
                return this.database.Shipments
                .Include(x => x.Destination)
                .ThenInclude(x => x.Address)
                .ThenInclude(x => x.City)
                .ThenInclude(x => x.Country)
                .Include(x => x.Origin)
                .ThenInclude(x => x.Address)
                .ThenInclude(x => x.City)
                .ThenInclude(x => x.Country)
                .Include(x => x.parcels);
            }
        }

        //Gets single shipment in db
        public Shipment GetById(int? id)
        {
            var shipment = this.Querry
                .Include(x => x.parcels)
                .ThenInclude(x => x.Customer)
                .Include(x => x.parcels)
                .ThenInclude(x => x.Category)
                .FirstOrDefault(x => x.ShipmentId.Equals(id));
            return shipment ?? throw new EntityNotFoundException();
        }

        //Gets all shipments in db
        public IEnumerable<ShipmentDto> GetAll()
        {
            var shipments = this.Querry;

            return ListModelByDTO(shipments);
        }

        //Creates shipment
        public Shipment Create(Shipment shipment)
        {
            var listOfParcels = new List<Parcel>();
            foreach (var x in shipment.parcels)
            {
                listOfParcels.Add(parcellService.Get(x.ParcelId));
            }
            shipment.parcels = listOfParcels;
            this.database.Shipments.Add(shipment);
            this.database.SaveChanges();
            return shipment;
        }

        //Updates existing shipment
        public Shipment Update(int id, Shipment newShipment)
        {
            var ShipmentToUpdate = this.database.Shipments
                .Include(x => x.parcels)
                .ThenInclude(x => x.Category)
                .FirstOrDefault(x => x.ShipmentId == id);
            if (ShipmentToUpdate.Status == Status.OnTheWay && newShipment.Status == Status.Completed)
            {
                ShipmentToUpdate.Status = newShipment.Status;
            }
            else if (ShipmentToUpdate.Status == Status.Preparing && (newShipment.Status == Status.OnTheWay || newShipment.Status == Status.Preparing))
            {
                ShipmentToUpdate.parcels = newShipment.parcels;
                ShipmentToUpdate.OriginWarehouseId = newShipment.OriginWarehouseId;
                ShipmentToUpdate.DestinationWarehouseId = newShipment.DestinationWarehouseId;
                ShipmentToUpdate.DepartureDate = newShipment.DepartureDate;
                ShipmentToUpdate.ArrivalDate = newShipment.ArrivalDate;
                ShipmentToUpdate.Status = newShipment.Status;
            }
            this.database.SaveChanges();

            return ShipmentToUpdate;
        }

        //Deletes existing shipment
        public void Delete(int id)
        {
            var shipment = this.database.Shipments
                .FirstOrDefault(x => x.ShipmentId == id);
            if (shipment == null)
            {
                throw new EntityNotFoundException();
            }
            shipment.isDeleted = true;
            this.database.SaveChanges();
        }

        //Filters shipment by the origin warehouse's id
        public IEnumerable<ShipmentDto> FilterWarehouse(int? id)
        {
            var shipments = this.Querry
                .Where(shipment => shipment.OriginWarehouseId == id);

            return ListModelByDTO(shipments);
        }

        //Get all shipments by a customer
        public IEnumerable<ShipmentDto> GetByCustomer(int? id)
        {
            var ShipmentList = database.Parcels
                .Include(x => x.Shipment)
                .ThenInclude(x => x.Destination)
                .ThenInclude(x => x.Address)
                .ThenInclude(x => x.City)
                .ThenInclude(x => x.Country)
                .Include(x => x.Shipment)
                .ThenInclude(x => x.Origin)
                .ThenInclude(x => x.Address)
                .ThenInclude(x => x.City)
                .ThenInclude(x => x.Country)
                .Where(x => x.CustomerId.Equals(id))
                .Select(x => x.Shipment);

            return ListModelByDTO(ShipmentList);
        }

        //Accepts list of shipment model and transforms it to shipmentDto model
        private IEnumerable<ShipmentDto> ListModelByDTO(IEnumerable<Shipment> shipments)
        {
            var shipmentDTOs = new List<ShipmentDto>();

            foreach (var shipment in shipments)
            {
                var shipmentDTO = new ShipmentDto(shipment);

                shipmentDTOs.Add(shipmentDTO);
            }
            return shipmentDTOs;
        }

        //Gets all other parcels that are in preparing status and not in the shipment that we are looking at
        public IEnumerable<ParcelDto> GetAllOtherParcels(int shipmentId, int OriginWarehouseIdFromShipment)
        {
            var parcelsList = this.database.Parcels
                .Include(x => x.Shipment)
                .Include(x => x.Customer)
                .Include(x => x.Category)
                .Where(x => x.ShipmentId == null && (x.WarehouseId == OriginWarehouseIdFromShipment || OriginWarehouseIdFromShipment == 0) || x.ShipmentId != shipmentId && x.Shipment.Status == Status.Preparing && x.WarehouseId == OriginWarehouseIdFromShipment);

            var dtoList = new List<ParcelDto>();
            foreach (var parcel in parcelsList)
            {
                var parcelDto = new ParcelDto(parcel);
                dtoList.Add(parcelDto);
            }
            return dtoList;
        }
    }
}
