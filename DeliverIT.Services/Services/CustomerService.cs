﻿using DeliverIt.Data;
using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using DeliverIT.Services.Tools;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IDatabase database;
        private readonly UniqueValidators validator;

        public CustomerService(IDatabase database, UniqueValidators validator)
        {
            this.database = database;
            this.validator = validator;
        }

        // Property for easy access to repeating querry
        private IQueryable<Customer> Querry
        {
            get
            {
                return this.database.Customers
                .Include(x => x.Address)
                .ThenInclude(x => x.City)
                .Include(x => x.Address)
                .ThenInclude(x => x.Country);
            }
        }

        // Gets one customer by id
        public Customer GetById(int? id)
        {
            var customer = this.Querry
                .FirstOrDefault(x => x.CustomerId.Equals(id));
            return customer ?? throw new EntityNotFoundException();
        }

        // Gets all customers in database
        public IEnumerable<CustomerDto> GetAll()
        {
            var customers = this.Querry.ToList();
            return ListModelByDTO(customers);
        }

        // Gets a count for a customers
        public int GetCount()
        {
            return database.Customers.Count();
        }

        // Creates new customer by validating his address and email and adding him in the database
        public Customer Create(Customer customer)
        {
            if (customer.Address != null)
            {
                var uniqueAddress = validator.ValidateAddress(customer.Address);
                customer.Address = uniqueAddress;
            }

            var uniqueEmail = validator.ValidateCustomerEmail(customer.Email);
            if (uniqueEmail)
            {
                throw new DuplicateEntityException();
            }
            this.database.Customers.Add(customer);
            this.database.SaveChanges();

            return customer;
        }

        // Updates a customer by validating addres and email. If the address is the same we assign it again using the already existing address.
        // If the email is the same we repeat it. But if the email matches an email of any other customer we throw an exception

        public Customer Update(int id, Customer customer)
        {

            var customerToUpdate = this.database.Customers
                .Include(x => x.Address)
                .FirstOrDefault(x => x.CustomerId.Equals(id));

            var uniqueAddress = validator.ValidateAddress(customer.Address);
            customer.Address = uniqueAddress;

            var uniqueEmail = validator.ValidateCustomerEmail(customer.Email);
            if (uniqueEmail)
            {
                if (customerToUpdate.Email != customer.Email)
                {
                    throw new DuplicateEntityException();
                }
            }
            customerToUpdate.FirstName = customer.FirstName;
            customerToUpdate.LastName = customer.LastName;
            customerToUpdate.Email = customer.Email;
            customerToUpdate.Address = customer.Address;
            customerToUpdate.Password = customer.Password;
            this.database.SaveChanges();

            return customerToUpdate;
        }

        // Deletes customer from database
        public void Delete(int id)
        {
            var customer = this.database.Customers
                .FirstOrDefault(x => x.CustomerId.Equals(id));
            if (customer == null)
            {
                throw new EntityNotFoundException();
            }
            customer.isDeleted = true;
            this.database.SaveChanges();
        }


        // Filters customer by first and last name.
        // Works with only one parameter too.
        public IEnumerable<CustomerDto> FilterByFirstAndLastName(string firstname, string lastname)
        {
            var filtered = this.Querry.ToList();

            if (firstname != null)
            {
                filtered = filtered
                    .Where(x => x.FirstName.Equals(firstname, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }

            if (lastname != null)
            {
                filtered = filtered
                    .Where(x => x.LastName.Equals(lastname, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }
            return ListModelByDTO(filtered);
        }

        // Searches all customers with similar email
        public IEnumerable<CustomerDto> GetByEmail(string email)
        {
            var customer = this.Querry
                .Where(customer => customer.Email.Contains(email));
            return ListModelByDTO(customer);
        }

        // Gets only one customer with exact email
        public Customer GetSingleCustomerByEmail(string email)
        {
            var customer = this.database.Customers
                   .FirstOrDefault(x => x.Email.ToLower().Equals(email.ToLower()));
            return customer;
        }

        // Gets all customers with one name (first or last)
        public IEnumerable<CustomerDto> GetByName(string name)
        {
            var customer = this.Querry
                .Where
                (customer => customer.FirstName.ToLower().Equals(name.ToLower()) || customer.LastName.ToLower().Equals(name.ToLower()));
            return ListModelByDTO(customer);
        }

        // Gets a customers incoming parcelss>
        public IEnumerable<ParcelDto> GetIncomingParcels(int? id)
        {
            var parcelsForCustomer = database.Shipments
            .Include(x => x.parcels)
            .ThenInclude(x => x.Category)
            .Include(x => x.parcels)
            .ThenInclude(x => x.Warehouse.Address)
            .ThenInclude(x => x.City)
            .Include(x => x.parcels)
            .ThenInclude(x => x.Warehouse.Address)
            .ThenInclude(x => x.Country)
            .Include(x => x.parcels)
            .ThenInclude(x => x.Customer)
            .Include(x => x.parcels)
            .ThenInclude(x => x.Shipment)
            .Select(x => x.parcels
            .Where(parcel => parcel.CustomerId.Equals(id) && parcel.Shipment.Status != Status.Completed));

            var parcelsDTOs = new List<ParcelDto>();
            foreach (var parcel in parcelsForCustomer)
            {
                foreach (var each in parcel)
                {
                    var dto = new ParcelDto(each);
                    parcelsDTOs.Add(dto);
                }
            }
            return parcelsDTOs;
        }

        // Gets a customers past parcels
        public IEnumerable<ParcelDto> GetPastParcels(int? id)
        {
            var parcelsForCustomer = database.Shipments
            .Include(x => x.parcels)
            .ThenInclude(x => x.Category)
            .Include(x => x.parcels)
            .ThenInclude(x => x.Warehouse.Address)
            .ThenInclude(x => x.City)
            .Include(x => x.parcels)
            .ThenInclude(x => x.Warehouse.Address)
            .ThenInclude(x => x.Country)
            .Include(x => x.parcels)
            .ThenInclude(x => x.Customer)
            .Include(x => x.parcels)
            .ThenInclude(x => x.Shipment)
            .Select(x => x.parcels.Where(parcel => parcel.CustomerId.Equals(id) && parcel.Shipment.Status.Equals(Status.Completed)));


            var parcelsDTOs = new List<ParcelDto>();
            foreach (var parcel in parcelsForCustomer)
            {
                foreach (var each in parcel)
                {
                    var dto = new ParcelDto(each);
                    parcelsDTOs.Add(dto);
                }
            }
            return parcelsDTOs;
        }


        // Changes the delivery status of a parcel if it hasnt been delivered yet.
        // If its on home delivery we change it to pickup from warehouse and vise versa.
        public string ChangeDeliveryStatus(int? Customerid, int parcelId)
        {
            var parcelOfCustomer = database.Parcels
                .Where(x => x.Shipment.Status != Status.Completed && x.CustomerId == Customerid && x.ParcelId == parcelId)
                .FirstOrDefault();
            var message = "Error";
            if (parcelOfCustomer.HomeDelivery == false)
            {
                parcelOfCustomer.HomeDelivery = true;
                message = "Delivery changed to home.";
            }
            else if (parcelOfCustomer.HomeDelivery == true)
            {
                parcelOfCustomer.HomeDelivery = false;
                message = "Delivery changed to warehouse.";

            }
            database.SaveChanges();
            return message;
        }

        // Searches email,firstname and lastname for a specific word
        public IEnumerable<CustomerDto> SearchAllFields(string searchWord)
        {
            if (searchWord == null)
            {
                throw new InvalidInputDataException();
            }
            var searched = GetByEmail(searchWord);
            foreach (var item in GetByName(searchWord))
            {
                if (!searched.Contains(item))
                {
                    searched.Append(item);
                }
            }
            return searched;
        }

        // Accepts a list of Customer Model and transforms it in a customer Dto
        private IEnumerable<CustomerDto> ListModelByDTO(IEnumerable<Customer> customers)
        {
            var customerDtoList = new List<CustomerDto>();

            foreach (var customer in customers)
            {
                var customerDto = new CustomerDto(customer);
                customerDtoList.Add(customerDto);
            }
            return customerDtoList;
        }
    }
}
