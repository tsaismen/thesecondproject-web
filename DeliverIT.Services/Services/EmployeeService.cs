﻿using DeliverIt.Data;
using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using DeliverIT.Services.Tools;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IDatabase database;
        private readonly UniqueValidators validator;

        public EmployeeService(IDatabase database, UniqueValidators validator)
        {
            this.database = database;
            this.validator = validator;
        }

        // Property for easy access to repeating querry
        private IQueryable<Employee> Querry
        {
            get
            {
                return this.database.Employees
                .Include(x => x.Address)
                .ThenInclude(x => x.City)
                .ThenInclude(x => x.Country);
            }
        }

        // Gets employee by id
        public Employee GetById(int? id)
        {
            var employee = this.Querry
                .FirstOrDefault(x => x.EmployeeId.Equals(id));
            return employee ?? throw new EntityNotFoundException();
        }

        // Gets all employees from database
        public IEnumerable<EmployeeDto> GetAll()
        {
            var employees = this.Querry.ToList();
            return ListModelByDTO(employees);
        }

        // Creates an employee.
        public Employee Create(Employee employee)
        {
            if (employee.Address != null)
            {
                var uniqueAddress = validator.ValidateAddress(employee.Address);
                employee.Address = uniqueAddress;
            }
            else
            {
                employee.AddressId = null;
            }
            var uniqueEmail = validator.ValidateEmployeeEmail(employee.Email);
            if (uniqueEmail)
            {
                throw new DuplicateEntityException("Employee with this email already exists!");
            }

            this.database.Employees.Add(employee);
            this.database.SaveChanges();

            return employee;
        }

        // Updates existing employee by id
        public Employee Update(int id, Employee employee)
        {
            var employeeToUpdate = this.database.Employees
                .Include(x => x.Address)
                .FirstOrDefault(x => x.EmployeeId.Equals(id));
            if (employee.Address != null)
            {
                var uniqueAddress = validator.ValidateAddress(employee.Address);
                employee.Address = uniqueAddress;
            }
            else
            {
                employee.AddressId = null;
            }
            var uniqueEmail = validator.ValidateEmployeeEmail(employee.Email);
            if (uniqueEmail)
            {
                if (employeeToUpdate.Email != employee.Email)
                {
                    throw new DuplicateEntityException();
                }
            }

            employeeToUpdate.FirstName = employee.FirstName;
            employeeToUpdate.LastName = employee.LastName;
            employeeToUpdate.Email = employee.Email;
            employeeToUpdate.Address = employee.Address;
            employeeToUpdate.WarehouseId = employee.WarehouseId;
            employeeToUpdate.Password = employee.Password;
            this.database.SaveChanges();

            return employeeToUpdate;
        }

        // Deletes employee
        public void Delete(int id)
        {
            var employee = this.database.Employees
                .FirstOrDefault(x => x.EmployeeId.Equals(id));
            if (employee == null)
            {
                throw new EntityNotFoundException();
            }
            employee.isDeleted = true;
            this.database.SaveChanges();
        }

        //Filters employees by first name and last name(works with only one of the paramaeters)
        public IEnumerable<EmployeeDto> FilterByFirstAndLastName(string firstname, string lastname)
        {
            var filtered = this.Querry.ToList();

            if (firstname != null)
            {
                filtered = filtered
                    .Where(x => x.FirstName.Equals(firstname, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }

            if (lastname != null)
            {
                filtered = filtered
                    .Where(x => x.LastName.Equals(lastname, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }
            return ListModelByDTO(filtered);
        }

        //Gets all employees wih similar emails
        public IEnumerable<EmployeeDto> GetByEmail(string email)
        {
            var employee = this.Querry
                .Where(customer => customer.Email.Contains(email));
            return ListModelByDTO(employee);
        }

        //Gets one employee with exact email
        public Employee GetSingleEmployeeByEmail(string email)
        {
            var employee = this.database.Employees
                   .FirstOrDefault(x => x.Email.ToLower().Equals(email.ToLower()));
            return employee;
        }

        //Gets all employees with similar names
        public IEnumerable<EmployeeDto> GetByName(string name)
        {
            var employee = this.Querry
                .Where
                (employee => employee.FirstName.ToLower().Equals(name.ToLower()) || employee.LastName.ToLower().Equals(name.ToLower()));
            return ListModelByDTO(employee);
        }

        //Gets all employees with a key word in their emails/names
        public IEnumerable<EmployeeDto> SearchAllFields(string searchWord)
        {
            var searched = GetByEmail(searchWord);
            foreach (var item in GetByName(searchWord))
            {
                if (!searched.Contains(item))
                {
                    searched.Append(item);
                }
            }
            return searched;
        }

        //Accepts list of Employee models and transforms them in employeeDto
        private IEnumerable<EmployeeDto> ListModelByDTO(IEnumerable<Employee> employees)
        {
            var employeeDTOs = new List<EmployeeDto>();

            foreach (var emp in employees)
            {
                var employeeDTO = new EmployeeDto(emp);

                employeeDTOs.Add(employeeDTO);
            }
            return employeeDTOs;
        }
    }
}
