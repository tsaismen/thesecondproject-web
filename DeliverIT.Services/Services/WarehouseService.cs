﻿using DeliverIt.Data;
using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services.Services
{
    public class WarehouseService : IWarehouseService
    {
        private readonly IDatabase database;

        public WarehouseService(IDatabase database)
        {
            this.database = database;
        }

        // Property for easy access to repeating querry
        private IQueryable<Warehouse> Querry
        {
            get
            {
                return this.database.Warehouses
                .Include(x => x.Address)
                .ThenInclude(x => x.City)
                .Include(x => x.Address)
                .ThenInclude(x => x.Country)
                .Include(x => x.DestinationShipments)
                .Include(x => x.OriginShipments);
            }
        }

        //Gets one warehouse
        public Warehouse Get(int? id)
        {
            var warehouse = this.Querry
                .FirstOrDefault(x => x.WarehouseId.Equals(id));
            return warehouse ?? throw new EntityNotFoundException();
        }
        //Gets all warehouses in db
        public IEnumerable<WarehouseDto> GetAll()
        {
            var warehouses = this.Querry
                .OrderBy(x => x.Address.Country.Name);

            var warehousesDTOs = new List<WarehouseDto>();
            foreach (var warehouse in warehouses)
            {
                var dto = new WarehouseDto(warehouse);
                warehousesDTOs.Add(dto);
            }
            return warehousesDTOs;
        }

        //Creates warehouse
        public Warehouse Create(Warehouse warehouse)
        {
            if (this.database.Warehouses.Any(x => x.Address.Street.Equals(warehouse.Address.Street)))
            {
                throw new DuplicateEntityException($"The warehouse on that location: {warehouse.Address.Street} already exists.");
            }

            this.database.Warehouses.Add(warehouse);
            database.SaveChanges();

            return warehouse;
        }

        //Updates existing warehouse
        public Warehouse Update(int id, Warehouse warehouse)
        {
            if (!this.database.Warehouses.Any(x => x.WarehouseId == id))
                throw new EntityNotFoundException();
            var warehouseToUpdate = this.database.Warehouses
                .Include(x => x.Address)
                .ThenInclude(x => x.Country)
                .Include(x => x.Address)
                .ThenInclude(x => x.City)
                .FirstOrDefault(x => x.WarehouseId == id);
            warehouseToUpdate.Address = warehouse.Address;

            database.SaveChanges();

            return warehouseToUpdate;
        }

        //Deletes existing warehouse
        public void Delete(int id)
        {
            var warehouse = this.database.Warehouses.FirstOrDefault(x => x.WarehouseId == id);
            if (warehouse == null)
                throw new EntityNotFoundException();
            warehouse.isDeleted = true;
            database.SaveChanges();
        }

        //Gets the next shipment that is coming to the specified warehouse (by warehouseId)
        public ShipmentDto GetNextOrder(int id)
        {
            if (!this.database.Warehouses.Any(x => x.WarehouseId == id))
                throw new EntityNotFoundException();

            var incomingShipment = database.Shipments
                .Include(x => x.Destination)
                .ThenInclude(x => x.Address)
                .ThenInclude(x => x.City)
                .ThenInclude(x => x.Country)
                .Include(x => x.Origin)
                .ThenInclude(x => x.Address)
                .ThenInclude(x => x.City)
                .ThenInclude(x => x.Country)
                .Where(x => x.DestinationWarehouseId == id)
                .OrderBy(x => x.ArrivalDate)
                .FirstOrDefault();

            var shipmentDTO = new ShipmentDto();
            if (incomingShipment == null)
            {
                return shipmentDTO;
            }

            shipmentDTO.ShipmentId = incomingShipment.ShipmentId;
            shipmentDTO.Origin = incomingShipment.Origin.Address.ToString();
            shipmentDTO.Destination = incomingShipment.Destination.Address.ToString();
            shipmentDTO.ArrivalDate = incomingShipment.ArrivalDate.ToShortDateString();
            shipmentDTO.DepartureDate = incomingShipment.DepartureDate.ToShortDateString();
            shipmentDTO.Status = incomingShipment.Status.ToString();

            return shipmentDTO;
        }

        //Gets all incoming shipments to the warehouse where the employee works at.
        public IEnumerable<ShipmentDto> GetIncomingShipments(int employeeId)
        {
            var incomingShipments = this.database.Employees
                .Include(x => x.Warehouse)
                .ThenInclude(x => x.DestinationShipments)
                .ThenInclude(x => x.Destination.Address)
                .ThenInclude(x => x.City)
                .ThenInclude(x => x.Country)
                .Include(x => x.Warehouse)
                .ThenInclude(x => x.DestinationShipments)
                .ThenInclude(x => x.Origin.Address)
                .ThenInclude(x => x.City)
                .ThenInclude(x => x.Country)
                .Where(x => x.EmployeeId.Equals(employeeId))
                .Select(x => x.Warehouse.DestinationShipments);

            var shipmentDtoList = new List<ShipmentDto>();
            foreach (var shipemnts in incomingShipments)
            {
                foreach (var shipment in shipemnts)
                {
                    var shipmentDto = new ShipmentDto(shipment);
                    shipmentDtoList.Add(shipmentDto);
                }
            }
            return shipmentDtoList;
        }
    }
}
