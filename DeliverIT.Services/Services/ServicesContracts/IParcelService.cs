﻿using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using System.Collections.Generic;

namespace DeliverIT.Services.Services.ServicesInterfaces
{
    public interface IParcelService
    {
        Parcel Get(int? id);
        IEnumerable<ParcelDto> Filter(int? customerid, string category, int? warehouseid, double? weight);
        IEnumerable<ParcelDto> SortBy(string weight, string arrival);
        IEnumerable<ParcelDto> GetAll();
        Parcel Create(Parcel parcel);
        Parcel Update(int id, Parcel parcel);
        void Delete(int id);
        IEnumerable<Category> GetAllCategories();
    }
}
