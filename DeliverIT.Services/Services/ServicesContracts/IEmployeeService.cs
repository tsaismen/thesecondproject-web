﻿using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using System.Collections.Generic;

namespace DeliverIT.Services.Services.ServicesInterfaces
{
    public interface IEmployeeService
    {
        Employee GetById(int? id);
        IEnumerable<EmployeeDto> GetAll();
        IEnumerable<EmployeeDto> GetByEmail(string email);
        Employee GetSingleEmployeeByEmail(string email);
        IEnumerable<EmployeeDto> GetByName(string name);
        IEnumerable<EmployeeDto> FilterByFirstAndLastName(string firstname, string lastname);
        IEnumerable<EmployeeDto> SearchAllFields(string searchWord);
        Employee Create(Employee employee);
        Employee Update(int id, Employee employee);
        void Delete(int id);
    }
}
