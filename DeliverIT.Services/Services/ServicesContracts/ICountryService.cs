﻿using DeliverIT.Models.AddressFolder;
using DeliverIT.Services.DTOs;
using System.Collections.Generic;

namespace DeliverIT.Services.Services.ServicesInterfaces
{
    public interface ICountryService
    {
        Country Get(int? id);
        IEnumerable<CountryDto> GetAll();
    }
}
