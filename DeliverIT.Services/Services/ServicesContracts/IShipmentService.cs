﻿using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using System.Collections.Generic;

namespace DeliverIT.Services.Services.ServicesInterfaces
{
    public interface IShipmentService
    {
        Shipment GetById(int? id);
        IEnumerable<ShipmentDto> GetAll();
        IEnumerable<ShipmentDto> FilterWarehouse(int? id);
        IEnumerable<ShipmentDto> GetByCustomer(int? id);
        Shipment Create(Shipment shipment);
        Shipment Update(int id, Shipment shipment);
        void Delete(int id);
        IEnumerable<ParcelDto> GetAllOtherParcels(int shipmentId, int OriginWarehouseIdFromShipment);
    }
}
