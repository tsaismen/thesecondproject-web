﻿using DeliverIT.Models.AddressFolder;
using DeliverIT.Services.DTOs;
using System.Collections.Generic;

namespace DeliverIT.Services.Services.ServicesInterfaces
{
    public interface ICityService
    {
        City Get(int? id);
        IEnumerable<CityDto> GetAll();
    }
}
