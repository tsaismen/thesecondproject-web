﻿using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using System.Collections.Generic;

namespace DeliverIT.Services.Services.ServicesInterfaces
{
    public interface IWarehouseService
    {
        Warehouse Get(int? id);
        ShipmentDto GetNextOrder(int id);
        IEnumerable<ShipmentDto> GetIncomingShipments(int employeeId);
        IEnumerable<WarehouseDto> GetAll();
        Warehouse Create(Warehouse warehouse);
        Warehouse Update(int id, Warehouse warehouse);
        void Delete(int id);
    }
}

