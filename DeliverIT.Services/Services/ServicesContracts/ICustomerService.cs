﻿using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using System.Collections.Generic;

namespace DeliverIT.Services.Services.ServicesInterfaces
{
    public interface ICustomerService
    {
        Customer GetById(int? id);
        IEnumerable<CustomerDto> GetAll();
        int GetCount();
        string ChangeDeliveryStatus(int? Customerid, int parcelId);
        IEnumerable<CustomerDto> GetByEmail(string email);
        Customer GetSingleCustomerByEmail(string email);
        IEnumerable<CustomerDto> GetByName(string name);
        IEnumerable<ParcelDto> GetIncomingParcels(int? id);
        IEnumerable<ParcelDto> GetPastParcels(int? id);
        IEnumerable<CustomerDto> FilterByFirstAndLastName(string firstname, string lastname);
        IEnumerable<CustomerDto> SearchAllFields(string searchWord);
        Customer Create(Customer customer);
        Customer Update(int id, Customer customer);
        void Delete(int id);
    }
}
