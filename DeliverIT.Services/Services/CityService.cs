﻿using DeliverIt.Data;
using DeliverIT.Models.AddressFolder;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services.Services
{
    public class CityService : ICityService
    {
        private readonly IDatabase database;

        public CityService(IDatabase database)
        {
            this.database = database;
        }

        //Returns one city by Id

        public City Get(int? id)
        {
            var city = this.database.Cities
                .Include(x => x.Country)
                .FirstOrDefault(x => x.CityId.Equals(id));
            return city ?? throw new EntityNotFoundException();
        }


        // Returns all cities in database
        public IEnumerable<CityDto> GetAll()
        {
            var cities = this.database.Cities
                .Include(c => c.Country);

            var cityDTOs = new List<CityDto>();

            foreach (var city in cities)
            {
                var dto = new CityDto(city);
                cityDTOs.Add(dto);
            }
            return cityDTOs;
        }
    }
}
