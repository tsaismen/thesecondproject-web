﻿using DeliverIt.Data;
using DeliverIT.Models.AddressFolder;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services.ServicesInterfaces;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services.Services
{
    public class CountrysService : ICountryService
    {
        private readonly IDatabase database;
        public CountrysService(IDatabase database)
        {
            this.database = database;
        }

        // Returns one country by id
        public Country Get(int? id)
        {
            var country = this.database.Countries
                .FirstOrDefault(x => x.CountryId == id);
            return country ?? throw new EntityNotFoundException();
        }

        // Returns all countries in database
        public IEnumerable<CountryDto> GetAll()
        {
            var countries = this.database.Countries;

            var CountriesDTOs = new List<CountryDto>();
            foreach (var country in countries)
            {
                var countryDTO = new CountryDto(country);
                CountriesDTOs.Add(countryDTO);
            }
            return CountriesDTOs;
        }
    }
}
