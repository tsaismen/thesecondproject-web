﻿using DeliverIt.Data;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Tests.Services
{
    [TestClass]
    public class Country_Should : BaseTest
    {
        private DbContextOptions<DeliveryDbContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new DeliveryDbContext(this.options))
            {
                arrangeContext.Countries.AddRange(Utils.GetCountries());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public void ReturnCorrectCity()
        {
            // Arrange        
            var testCountry = new CountryDto
            {
                CountryName = "Bulgaria"
            };

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new CountrysService(assertContext);
                var resultCountry = sut.Get(1);

                Assert.AreEqual(testCountry.CountryName, resultCountry.Name);
            }
        }


        [TestMethod]
        public void ReturnAllCities()
        {
            // Arrange        
            var testCity1 = new CountryDto
            {
                CountryName = "Bulgaria"
            };
            var testCity2 = new CountryDto
            {
                CountryName = "France"
            };

            List<CountryDto> expectedCountries = new List<CountryDto>();
            expectedCountries.Add(testCity1);
            expectedCountries.Add(testCity2);

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new CountrysService(assertContext);
                var resultCities = sut.GetAll().ToList();

                for (int i = 0; i < resultCities.Count; i++)
                {
                    Assert.AreEqual(expectedCountries[i].CountryName, resultCities[i].CountryName);
                }
            }
        }

        [TestMethod]
        public void ThrowException_When_CityNotFound()
        {
            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new CountrysService(assertContext);
                int nonExistentId = int.MaxValue;

                Assert.ThrowsException<EntityNotFoundException>(() => sut.Get(nonExistentId));
            }
        }
    }
}
