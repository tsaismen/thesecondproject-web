﻿using DeliverIt.Data;
using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services;
using DeliverIT.Services.Tools;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
namespace DeliverIT.Tests.Services
{
    [TestClass]
    public class Customer_Should : BaseTest
    {
        private DbContextOptions<DeliveryDbContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new DeliveryDbContext(this.options))
            {
                arrangeContext.Customers.AddRange(Utils.GetCustomers());
                arrangeContext.Addresses.AddRange(Utils.GetAddresses());
                arrangeContext.Cities.AddRange(Utils.GetCities());
                arrangeContext.Countries.AddRange(Utils.GetCountries());
                arrangeContext.Parcels.AddRange(Utils.GetParcels());
                arrangeContext.Shipments.AddRange(Utils.GetShipments());
                arrangeContext.Categories.AddRange(Utils.GetCategories());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public void ReturnCorrectCustomer()
        {
            // Arrange        
            var testCustomer = new CustomerDto
            {
                CustomerId = 1,
                FirstName = "Ivailo",
                LastName = "Petrov",
                Email = "ivakamadafaka@gmail.com",
                Address = "Baguette 32, Paris, France"
            };

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new CustomerService(assertContext, validator);
                var resultCustomer = sut.GetById(1);

                Assert.AreEqual(testCustomer.FirstName, resultCustomer.FirstName);
                Assert.AreEqual(testCustomer.LastName, resultCustomer.LastName);
                Assert.AreEqual(testCustomer.Email, resultCustomer.Email);
                Assert.AreEqual(testCustomer.Address, resultCustomer.Address.ToString());
            }
        }


        [TestMethod]
        public void ReturnAllCustomers()
        {
            // Arrange        
            var testCustomer1 = new CustomerDto
            {
                CustomerId = 1,
                FirstName = "Ivailo",
                LastName = "Petrov",
                Email = "ivakamadafaka@gmail.com",
                Address = "Baguette 32, Paris, France"
            };
            var testCustomer2 = new CustomerDto
            {
                CustomerId = 2,
                FirstName = "Stanko",
                LastName = "Petrov",
                Email = "stankoteslata@gmail.com",
                Address = "Kelesho 32, Paris, France"
            };

            List<CustomerDto> testCustomers = new List<CustomerDto>();
            testCustomers.Add(testCustomer1);
            testCustomers.Add(testCustomer2);

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new CustomerService(assertContext, validator);
                var resultCustomers = sut.GetAll().ToList();

                for (int i = 0; i < resultCustomers.Count; i++)
                {
                    Assert.AreEqual(testCustomers[i].FirstName, resultCustomers[i].FirstName);
                    Assert.AreEqual(testCustomers[i].LastName, resultCustomers[i].LastName);
                    Assert.AreEqual(testCustomers[i].Email, resultCustomers[i].Email);
                    Assert.AreEqual(testCustomers[i].Address, resultCustomers[i].Address);
                }
            }
        }

        [TestMethod]
        public void GetsCorrectCount()
        {
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new CustomerService(assertContext, validator);
                var count = sut.GetCount();
                Assert.AreEqual(count, 2);
            }
        }

        [TestMethod]
        public void ThrowException_When_ShipmentNotFound()
        {
            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new CustomerService(assertContext, validator);
                int nonExistentId = int.MaxValue;

                Assert.ThrowsException<EntityNotFoundException>(() => sut.GetById(nonExistentId));
            }
        }

        [TestMethod]
        public void PostThrowsIfEmailExsists()
        {
            // Arrange

            var Customer3 = new Customer()
            {
                CustomerId = 3,
                FirstName = "Divan",
                LastName = "Petrov",
                Email = "stankoteslata@gmail.com",
                AddressId = 1,
                Address = new Address()
                {
                    Street = "Baba Tonka 32"
                },
                Parcels = null,
                isDeleted = false
            };

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new CustomerService(assertContext, validator);
                Assert.ThrowsException<DuplicateEntityException>(() => sut.Create(Customer3));

                //Assert.AreEqual(expected, Customer3);
            }
        }

        [TestMethod]
        public void PostCreatesCorrectly()
        {
            // Arrange

            var Customer3 = new Customer()
            {
                CustomerId = 3,
                FirstName = "Divan",
                LastName = "Petrov",
                Email = "dip@gmail.com",
                AddressId = 1,
                Address = new Address()
                {
                    Street = "Baba Tonka 32"
                },
                Parcels = null,
                isDeleted = false
            };

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new CustomerService(assertContext, validator);
                var resultCustomer = sut.Create(Customer3);
                //var expected = sut.GetById(3);

                Assert.AreEqual(resultCustomer.FirstName, Customer3.FirstName);
                Assert.AreEqual(resultCustomer.LastName, Customer3.LastName);
                Assert.AreEqual(resultCustomer.Email, Customer3.Email);
            }
        }

        [TestMethod]
        public void UpdateCustomerCorrctly()
        {
            // Arrange

            var Customer2 = new Customer()
            {
                CustomerId = 2,
                FirstName = "Divan",
                LastName = "Petrov",
                Email = "dip@gmail.com",
                AddressId = 1,
                Address = new Address()
                {
                    Street = "Baba Tonka 32"
                },
                Parcels = null,
                isDeleted = false
            };

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new CustomerService(assertContext, validator);
                var resultCustomer = sut.Update(2, Customer2);

                Assert.AreEqual(resultCustomer.FirstName, Customer2.FirstName);
                Assert.AreEqual(resultCustomer.LastName, Customer2.LastName);
                Assert.AreEqual(resultCustomer.Email, Customer2.Email);
            }
        }

        [TestMethod]
        public void UpdateCustomerThrowsIfEmailExists()
        {
            // Arrange

            var Customer2 = new Customer()
            {
                CustomerId = 2,
                FirstName = "Divan",
                LastName = "Petrov",
                Email = "stankoteslata@gmail.com",
                AddressId = 1,
                Address = new Address()
                {
                    Street = "Baba Tonka 32"
                },
                Parcels = null,
                isDeleted = false
            };

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new CustomerService(assertContext, validator);

                Assert.ThrowsException<DuplicateEntityException>(() => sut.Update(1, Customer2));
            }
        }

        [TestMethod]
        public void DeleteCustomerCorrectly()
        {
            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new CustomerService(assertContext, validator);
                sut.Delete(2);
                Assert.ThrowsException<EntityNotFoundException>(() => sut.GetById(2));
                Assert.ThrowsException<EntityNotFoundException>(() => sut.Delete(3));

            }
        }




        [TestMethod]
        public void GetCustomerByNameOnly()
        {
            // Arrange
            var customer1 = new Customer
            {
                CustomerId = 1,
                FirstName = "Ivailo",
                LastName = "Petrov",
                Email = "ivakamadafaka@gmail.com",
                AddressId = 3
            };
            var customer2 = new Customer
            {
                CustomerId = 2,
                FirstName = "Stanko",
                LastName = "Petrov",
                Email = "stankoteslata@gmail.com",
                AddressId = 4

            };

            List<Customer> customerList = new List<Customer>();
            customerList.Add(customer1);
            customerList.Add(customer2);

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new CustomerService(assertContext, validator);
                var resultCustomerList = sut.GetByName("Petrov").ToList();
                //var expected = sut.GetById(3);

                for (int i = 0; i < resultCustomerList.Count; i++)
                {
                    Assert.AreEqual(resultCustomerList[i].FirstName, customerList[i].FirstName);
                    Assert.AreEqual(resultCustomerList[i].LastName, customerList[i].LastName);
                    Assert.AreEqual(resultCustomerList[i].Email, customerList[i].Email);
                }

            }
        }

        [TestMethod]
        public void GetCustomerByFirstOrLast()
        {
            // Arrange
            var customer1 = new Customer
            {
                CustomerId = 1,
                FirstName = "Ivailo",
                LastName = "Petrov",
                Email = "ivakamadafaka@gmail.com",
                AddressId = 3
            };
            var customer2 = new Customer
            {
                CustomerId = 2,
                FirstName = "Stanko",
                LastName = "Petrov",
                Email = "stankoteslata@gmail.com",
                AddressId = 4

            };

            List<Customer> customerList = new List<Customer>();
            customerList.Add(customer1);

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new CustomerService(assertContext, validator);
                var resultCustomerList = sut.FilterByFirstAndLastName("Ivailo", "Petrov").ToList();
                //var expected = sut.GetById(3);

                for (int i = 0; i < resultCustomerList.Count; i++)
                {
                    Assert.AreEqual(resultCustomerList[i].FirstName, customerList[i].FirstName);
                    Assert.AreEqual(resultCustomerList[i].LastName, customerList[i].LastName);
                    Assert.AreEqual(resultCustomerList[i].Email, customerList[i].Email);
                }

            }
        }

        [TestMethod]
        public void GetCustomerByFullEmail()
        {
            // Arrange
            var customer1 = new Customer
            {
                CustomerId = 1,
                FirstName = "Ivailo",
                LastName = "Petrov",
                Email = "ivakamadafaka@gmail.com",
                AddressId = 3
            };
            var customer2 = new Customer
            {
                CustomerId = 2,
                FirstName = "Stanko",
                LastName = "Petrov",
                Email = "stankoteslata@gmail.com",
                AddressId = 4

            };

            List<Customer> customerList = new List<Customer>();
            customerList.Add(customer2);

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new CustomerService(assertContext, validator);
                var resultCustomerList = sut.GetSingleCustomerByEmail("stankoteslata@gmail.com");
                //var expected = sut.GetById(3);

                Assert.AreEqual(resultCustomerList.FirstName, customerList[0].FirstName);
                Assert.AreEqual(resultCustomerList.LastName, customerList[0].LastName);
                Assert.AreEqual(resultCustomerList.Email, customerList[0].Email);

            }
        }

        [TestMethod]
        public void GetCustomerByPartEmail()
        {
            // Arrange
            var customer1 = new Customer
            {
                CustomerId = 1,
                FirstName = "Ivailo",
                LastName = "Petrov",
                Email = "ivakamadafaka@gmail.com",
                AddressId = 3
            };
            var customer2 = new Customer
            {
                CustomerId = 2,
                FirstName = "Stanko",
                LastName = "Petrov",
                Email = "stankoteslata@gmail.com",
                AddressId = 4

            };

            List<Customer> customerList = new List<Customer>();
            customerList.Add(customer1);
            customerList.Add(customer2);

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new CustomerService(assertContext, validator);
                var resultCustomerList = sut.GetByEmail("gmail.com").ToList();
                //var expected = sut.GetById(3);

                for (int i = 0; i < resultCustomerList.Count; i++)
                {
                    Assert.AreEqual(resultCustomerList[i].FirstName, customerList[i].FirstName);
                    Assert.AreEqual(resultCustomerList[i].LastName, customerList[i].LastName);
                    Assert.AreEqual(resultCustomerList[i].Email, customerList[i].Email);
                }

            }
        }

        [TestMethod]
        public void GetIncomingParcelsForCustomer()
        {

            var parcel = new ParcelDto
            {
                Weight = 5,
                Category = "Clothes",
                CustomerName = "Ivailo Petrov"
            };


            var parcelList = new List<ParcelDto> { parcel };

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new CustomerService(assertContext, validator);
                var resultParcelsList = sut.GetIncomingParcels(1).ToList();//fails

                for (int i = 0; i < resultParcelsList.Count; i++)
                {
                    Assert.AreEqual(resultParcelsList[i].Weight, parcelList[i].Weight);
                    Assert.AreEqual(resultParcelsList[i].Category, parcelList[i].Category);
                    Assert.AreEqual(resultParcelsList[i].CustomerName, parcelList[i].CustomerName);
                }
            }
        }

        [TestMethod]
        public void SearchAllFindsCorrectValues()
        {

            var customer2 = new Customer
            {
                CustomerId = 2,
                FirstName = "Stanko",
                LastName = "Petrov",
                Email = "stankoteslata@gmail.com",
                AddressId = 4
            };

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new CustomerService(assertContext, validator);
                var resultParcelsList = sut.SearchAllFields("stanko").ToList();

                Assert.AreEqual(resultParcelsList[0].FirstName, customer2.FirstName);
                Assert.AreEqual(resultParcelsList[0].LastName, customer2.LastName);
                Assert.AreEqual(resultParcelsList[0].Email, customer2.Email);
            }
        }


        [TestMethod]
        public void ChangeDeliveryStatus()
        {
            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new CustomerService(assertContext, validator);
                var resultParcelsList = sut.ChangeDeliveryStatus(1, 3);

                Assert.AreEqual("Delivery changed to home.", resultParcelsList);

                resultParcelsList = sut.ChangeDeliveryStatus(1, 3);

                Assert.AreEqual("Delivery changed to warehouse.", resultParcelsList);
            }
        }
    }
}
