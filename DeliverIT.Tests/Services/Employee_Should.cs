﻿using DeliverIt.Data;
using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services;
using DeliverIT.Services.Tools;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Tests.Services
{
    [TestClass]
    public class Employee_Should : BaseTest
    {
        private DbContextOptions<DeliveryDbContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new DeliveryDbContext(this.options))
            {
                arrangeContext.Employees.AddRange(Utils.GetEmployees());
                arrangeContext.Addresses.AddRange(Utils.GetAddresses());
                arrangeContext.Cities.AddRange(Utils.GetCities());
                arrangeContext.Countries.AddRange(Utils.GetCountries());
                arrangeContext.Parcels.AddRange(Utils.GetParcels());
                arrangeContext.Shipments.AddRange(Utils.GetShipments());
                arrangeContext.Categories.AddRange(Utils.GetCategories());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public void ReturnCorrectEmployee()
        {
            // Arrange        
            var testCustomer = new EmployeeDto
            {
                EmployeeId = 1,
                FirstName = "Georgi",
                LastName = "Ivanov",
                Email = "georgiivanov23@gmail.com",
                Address = "Baba Tonka 32, Montana, Bulgaria"
            };

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new EmployeeService(assertContext, validator);
                var resultCustomer = sut.GetById(1);

                Assert.AreEqual(testCustomer.FirstName, resultCustomer.FirstName);
                Assert.AreEqual(testCustomer.LastName, resultCustomer.LastName);
                Assert.AreEqual(testCustomer.Email, resultCustomer.Email);
                Assert.AreEqual(testCustomer.Address, resultCustomer.Address.ToString());
            }
        }


        [TestMethod]
        public void ReturnAllEmployes()
        {
            // Arrange        
            var testEmployee1 = new EmployeeDto
            {
                EmployeeId = 1,
                FirstName = "Georgi",
                LastName = "Ivanov",
                Email = "georgiivanov23@gmail.com",
                Address = "Baba Tonka 32, Montana, Bulgaria"
            };
            var testEmployee2 = new EmployeeDto
            {
                EmployeeId = 2,
                FirstName = "Parvan",
                LastName = "Stanislavov",
                Email = "parata@gmail.com",
                Address = "Patlajan 32, Montana, Bulgaria",
            };

            List<EmployeeDto> testEmployees = new List<EmployeeDto>();
            testEmployees.Add(testEmployee1);
            testEmployees.Add(testEmployee2);

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new EmployeeService(assertContext, validator);
                var resultEmployees = sut.GetAll().ToList();

                for (int i = 0; i < resultEmployees.Count; i++)
                {
                    Assert.AreEqual(testEmployees[i].FirstName, resultEmployees[i].FirstName);
                    Assert.AreEqual(testEmployees[i].LastName, resultEmployees[i].LastName);
                    Assert.AreEqual(testEmployees[i].Email, resultEmployees[i].Email);
                    Assert.AreEqual(testEmployees[i].Address, resultEmployees[i].Address);
                }
            }
        }


        [TestMethod]
        public void PostThrowsIfEmailExsists()
        {
            // Arrange

            var employee = new Employee()
            {
                EmployeeId = 2,
                FirstName = "Divan",
                LastName = "Petrov",
                Email = "georgiivanov23@gmail.com",
                isDeleted = false
            };

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new EmployeeService(assertContext, validator);
                Assert.ThrowsException<DuplicateEntityException>(() => sut.Create(employee));

                //Assert.AreEqual(expected, Customer3);
            }
        }

        [TestMethod]
        public void PostCreatesCorrectly()
        {
            // Arrange

            var employee = new Employee()
            {
                FirstName = "Georgi",
                LastName = "Ivanov",
                Email = "georgiivan@gmail.com",
                isDeleted = false
            };

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new EmployeeService(assertContext, validator);
                var resultCustomer = sut.Create(employee);
                //var expected = sut.GetById(3);

                Assert.AreEqual(resultCustomer.FirstName, employee.FirstName);
                Assert.AreEqual(resultCustomer.LastName, employee.LastName);
                Assert.AreEqual(resultCustomer.Email, employee.Email);
            }
        }

        [TestMethod]
        public void UpdateCorrectly()
        {
            // Arrange

            var employee = new Employee()
            {
                FirstName = "Georgi",
                LastName = "Ivanov",
                Email = "georgiivan@gmail.com",
                isDeleted = false
            };
            var employee2 = new Employee()
            {
                FirstName = "Georgi",
                LastName = "Ivanov",
                Email = "georgiivanov23@gmail.com",
                isDeleted = false
            };

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new EmployeeService(assertContext, validator);
                var resultCustomer = sut.Update(2, employee);

                Assert.AreEqual(resultCustomer.FirstName, employee.FirstName);
                Assert.AreEqual(resultCustomer.LastName, employee.LastName);
                Assert.AreEqual(resultCustomer.Email, employee.Email);
                Assert.ThrowsException<DuplicateEntityException>(() => sut.Update(2, employee2));
            }
        }


        [TestMethod]
        public void DeleteCorrectly()
        {
            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new EmployeeService(assertContext, validator);
                sut.Delete(2);
                Assert.ThrowsException<EntityNotFoundException>(() => sut.GetById(2));
                Assert.ThrowsException<EntityNotFoundException>(() => sut.Delete(3));

            }
        }

        [TestMethod]
        public void GetByNameOnly()
        {
            // Arrange
            var customer1 = new Customer
            {
                CustomerId = 1,
                FirstName = "Ivailo",
                LastName = "Petrov",
                Email = "ivakamadafaka@gmail.com",
                AddressId = 3
            };
            var customer2 = new Customer
            {
                CustomerId = 2,
                FirstName = "Stanko",
                LastName = "Petrov",
                Email = "stankoteslata@gmail.com",
                AddressId = 4

            };

            List<Customer> customerList = new List<Customer>();
            customerList.Add(customer1);
            customerList.Add(customer2);

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new CustomerService(assertContext, validator);
                var resultCustomerList = sut.GetByName("Petrov").ToList();
                //var expected = sut.GetById(3);

                for (int i = 0; i < resultCustomerList.Count; i++)
                {
                    Assert.AreEqual(resultCustomerList[i].FirstName, customerList[i].FirstName);
                    Assert.AreEqual(resultCustomerList[i].LastName, customerList[i].LastName);
                    Assert.AreEqual(resultCustomerList[i].Email, customerList[i].Email);
                }

            }
        }

        [TestMethod]
        public void GetByFirstOrLast()
        {
            // Arrange
            var employee = new Employee
            {
                EmployeeId = 1,
                FirstName = "Georgi",
                LastName = "Ivanov",
                Email = "georgiivanov23@gmail.com",
                AddressId = 1
            };

            List<Employee> testEmployeeList = new List<Employee>();
            testEmployeeList.Add(employee);

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new EmployeeService(assertContext, validator);
                var resultEmployeeList = sut.FilterByFirstAndLastName("Georgi", "Ivanov").ToList();
                //var expected = sut.GetById(3);

                for (int i = 0; i < resultEmployeeList.Count; i++)
                {
                    Assert.AreEqual(resultEmployeeList[i].FirstName, testEmployeeList[i].FirstName);
                    Assert.AreEqual(resultEmployeeList[i].LastName, testEmployeeList[i].LastName);
                    Assert.AreEqual(resultEmployeeList[i].Email, testEmployeeList[i].Email);
                }

            }
        }

        [TestMethod]
        public void GetEmployeeByFullEmail()
        {
            // Arrange
            var employee = new Employee
            {
                EmployeeId = 1,
                FirstName = "Georgi",
                LastName = "Ivanov",
                Email = "georgiivanov23@gmail.com",
                AddressId = 1
            };


            List<Employee> employeeList = new List<Employee>();
            employeeList.Add(employee);

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new EmployeeService(assertContext, validator);
                var resultEmployee = sut.GetSingleEmployeeByEmail("georgiivanov23@gmail.com");
                //var expected = sut.GetById(3);

                Assert.AreEqual(resultEmployee.FirstName, employeeList[0].FirstName);
                Assert.AreEqual(resultEmployee.LastName, employeeList[0].LastName);
                Assert.AreEqual(resultEmployee.Email, employeeList[0].Email);

            }
        }

        [TestMethod]
        public void GetEmployeeByPartEmail()
        {
            // Arrange
            var employee = new Employee
            {
                EmployeeId = 1,
                FirstName = "Georgi",
                LastName = "Ivanov",
                Email = "georgiivanov23@gmail.com",
                AddressId = 1
            };
            var employee2 = new Employee
            {
                EmployeeId = 2,
                FirstName = "Parvan",
                LastName = "Stanislavov",
                Email = "parata@gmail.com",
                AddressId = 2
            };


            List<Employee> testEmployeeList = new List<Employee>();
            testEmployeeList.Add(employee);
            testEmployeeList.Add(employee2);

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new EmployeeService(assertContext, validator);
                var resultEmployeeList = sut.GetByEmail("gmail.com").ToList();
                //var expected = sut.GetById(3);

                for (int i = 0; i < resultEmployeeList.Count; i++)
                {
                    Assert.AreEqual(resultEmployeeList[i].FirstName, testEmployeeList[i].FirstName);
                    Assert.AreEqual(resultEmployeeList[i].LastName, testEmployeeList[i].LastName);
                    Assert.AreEqual(resultEmployeeList[i].Email, testEmployeeList[i].Email);
                }

            }
        }

        [TestMethod]
        public void SearchAllFindsCorrectValues()
        {

            var employee = new Employee
            {
                EmployeeId = 1,
                FirstName = "Georgi",
                LastName = "Ivanov",
                Email = "georgiivanov23@gmail.com",
                AddressId = 1
            };

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var validator = new UniqueValidators(assertContext);
                var sut = new EmployeeService(assertContext, validator);
                var resultEmployee = sut.SearchAllFields("Ivanov").ToList();

                for (var i = 0; i < resultEmployee.Count(); i++)
                {
                    Assert.AreEqual(resultEmployee[0].FirstName, employee.FirstName);
                    Assert.AreEqual(resultEmployee[0].LastName, employee.LastName);
                    Assert.AreEqual(resultEmployee[0].Email, employee.Email);
                }
            }
        }
    }
}
