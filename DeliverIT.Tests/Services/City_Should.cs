﻿using DeliverIt.Data;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Tests.Services
{
    [TestClass]
    public class City_Should : BaseTest
    {
        private DbContextOptions<DeliveryDbContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new DeliveryDbContext(this.options))
            {
                arrangeContext.Cities.AddRange(Utils.GetCities());
                arrangeContext.Countries.AddRange(Utils.GetCountries());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public void ReturnCorrectCity()
        {
            // Arrange        
            var testCity = new CityDto
            {
                City = "Montana",
                Country = "Bulgaria"
            };

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new CityService(assertContext);
                var resultCity = sut.Get(1);

                Assert.AreEqual(testCity.City, resultCity.Name);
                Assert.AreEqual(testCity.Country, resultCity.Country.Name);
            }
        }


        [TestMethod]
        public void ReturnAllCities()
        {
            // Arrange        
            var testCity = new CityDto
            {
                City = "Montana",
                Country = "Bulgaria"
            };
            var testCity2 = new CityDto
            {
                City = "Paris",
                Country = "France"
            };

            List<CityDto> expectedCities = new List<CityDto>();
            expectedCities.Add(testCity);
            expectedCities.Add(testCity2);

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new CityService(assertContext);
                var resultCities = sut.GetAll().ToList();

                for (int i = 0; i < resultCities.Count; i++)
                {
                    Assert.AreEqual(expectedCities[i].City, resultCities[i].City);
                    Assert.AreEqual(expectedCities[i].Country, resultCities[i].Country);
                }
            }
        }

        [TestMethod]
        public void ThrowException_When_CityNotFound()
        {
            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new CityService(assertContext);
                int nonExistentId = int.MaxValue;

                Assert.ThrowsException<EntityNotFoundException>(() => sut.Get(nonExistentId));
            }
        }
    }
}
