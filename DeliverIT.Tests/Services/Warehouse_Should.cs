﻿using DeliverIt.Data;
using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Tests.Services
{
    [TestClass]
    public class Warehouse_Should : BaseTest
    {
        private DbContextOptions<DeliveryDbContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new DeliveryDbContext(this.options))
            {
                arrangeContext.Cities.AddRange(Utils.GetCities());
                arrangeContext.Countries.AddRange(Utils.GetCountries());
                arrangeContext.Shipments.AddRange(Utils.GetShipments());
                arrangeContext.Warehouses.AddRange(Utils.GetWarehouses());
                arrangeContext.Addresses.AddRange(Utils.GetAddresses());
                arrangeContext.SaveChanges();
            }
        }
        [TestMethod]
        public void ReturnCorrectWarehouse()
        {
            // Arrange        
            var testWarehouse = new WarehouseDto
            {
                Address = "Baba Tonka 32, Montana, Bulgaria"
            };

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new WarehouseService(assertContext);
                var resultWarehouse = sut.Get(1);

                Assert.AreEqual(testWarehouse.Address, resultWarehouse.Address.ToString());
            }

        }
        [TestMethod]
        public void ThrowExceptionWhenWarehouseIdIsWrong()
        {
            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new WarehouseService(assertContext);
                int nonExistentId = 10;

                Assert.ThrowsException<EntityNotFoundException>(() => sut.Get(nonExistentId));
            }
        }

        [TestMethod]
        public void ReturnAllWarehouses()
        {
            var testWarehouses = new List<WarehouseDto>() {
                new WarehouseDto
                {
                    Address="Baba Tonka 32, Montana, Bulgaria"
                },
                new WarehouseDto
                {
                    Address="Patlajan 32, Montana, Bulgaria"
                }
            };

            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new WarehouseService(assertContext);
                var resultWarehouses = sut.GetAll().ToList();

                for (var i = 0; i < resultWarehouses.Count; i++)
                {
                    Assert.AreEqual(testWarehouses[i].Address, resultWarehouses[i].Address);
                }
            }
        }
        [TestMethod]
        public void ShouldCreateWarehouse()
        {
            // Arrange        
            var testWarehouse = new Warehouse
            {
                Address = new Address()
                {
                    Street = "Sveti Iliq 55",
                    CityId = 1,
                    CountryId = 1
                }
            };
            var testWarehouse2 = new Warehouse
            {
                Address = new Address()
                {
                    Street = "Sveti Iliq 55",
                    CityId = 1,
                    CountryId = 1
                }
            };
            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new WarehouseService(assertContext);
                var newParcel = sut.Create(testWarehouse);
                var resultWarehouse = sut.Get(3);

                Assert.AreEqual(testWarehouse.Address.ToString(), resultWarehouse.Address.ToString());
                Assert.ThrowsException<DuplicateEntityException>(() => sut.Create(testWarehouse2));
            }

        }
        [TestMethod]
        public void ShouldUpdateWarehouse()
        {
            // Arrange        
            var testWarehouse = new Warehouse
            {
                Address = new Address()
                {
                    Street = "Sveti Iliq 55",
                    CityId = 1,
                    CountryId = 1
                }
            };
            var testWarehouse2 = new Warehouse
            {
                Address = new Address()
                {
                    Street = "Sveti Iliq 22",
                    CityId = 1,
                    CountryId = 1
                }
            };
            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new WarehouseService(assertContext);
                var newParcel = sut.Create(testWarehouse);
                sut.Update(3, testWarehouse2);
                var resultWarehouse = sut.Get(3);

                Assert.AreEqual("Sveti Iliq 22, Montana, Bulgaria", resultWarehouse.Address.ToString());
                Assert.ThrowsException<EntityNotFoundException>(() => sut.Update(4, testWarehouse2));
            }
        }
        [TestMethod]
        public void DeleteShould()
        {
            // Arrange 
            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new WarehouseService(assertContext);
                sut.Delete(2);

                Assert.ThrowsException<EntityNotFoundException>(() => sut.Get(2));
                Assert.ThrowsException<EntityNotFoundException>(() => sut.Delete(2));
            }

        }
        [TestMethod]
        public void ShouldReturnNextShipment()
        {
            var testShipment = new ShipmentDto
            {
                Destination = "Patlajan 32, Montana, Bulgaria",
                Status = "Preparing"
            };


            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new WarehouseService(assertContext);
                var resultWarehouses = sut.GetNextOrder(2);

                Assert.AreEqual(testShipment.Destination, resultWarehouses.Destination);
                Assert.AreEqual(testShipment.Status, resultWarehouses.Status);
                Assert.ThrowsException<EntityNotFoundException>(() => sut.GetNextOrder(6));
            }
        }
    }
}
