﻿using DeliverIt.Data;
using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Tests.Services
{
    [TestClass]
    public class Shipment_Should : BaseTest
    {

        private DbContextOptions<DeliveryDbContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new DeliveryDbContext(this.options))
            {
                arrangeContext.Shipments.AddRange(Utils.GetShipments());
                arrangeContext.Addresses.AddRange(Utils.GetAddresses());
                arrangeContext.Cities.AddRange(Utils.GetCities());
                arrangeContext.Countries.AddRange(Utils.GetCountries());
                arrangeContext.Categories.AddRange(Utils.GetCategories());
                arrangeContext.Parcels.AddRange(Utils.GetParcels());
                arrangeContext.Shipments.AddRange(Utils.GetShipments());
                arrangeContext.Warehouses.AddRange(Utils.GetWarehouses());
                arrangeContext.SaveChanges();
            }
        }


        [TestMethod]
        public void ReturnCorrectShipment()
        {
            // Arrange        
            var testShiment = new ShipmentDto
            {
                Destination = "Patlajan 32, Montana, Bulgaria",
                ArrivalDate = DateTime.Now.AddDays(1).ToShortDateString(),
                Status = Status.Preparing.ToString()
            };

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var parService = new ParcelService(assertContext);
                var sut = new ShipmentService(assertContext, parService);
                var resultShipment = sut.GetById(1);

                Assert.AreEqual(testShiment.Destination, resultShipment.Destination.Address.ToString());
                Assert.AreEqual(testShiment.ArrivalDate, resultShipment.ArrivalDate.ToShortDateString());
                Assert.AreEqual(testShiment.Status, resultShipment.Status.ToString());
            }
        }


        [TestMethod]
        public void ReturnAllShipments()
        {
            // Arrange        
            var testShiment1 = new ShipmentDto
            {
                Destination = "Patlajan 32, Montana, Bulgaria",
                ArrivalDate = DateTime.Now.AddDays(1).ToShortDateString(),
                Status = Status.Preparing.ToString()
            }; var testShiment2 = new ShipmentDto
            {
                Destination = "Baba Tonka 32, Montana, Bulgaria",
                ArrivalDate = DateTime.Now.AddDays(3).ToShortDateString(),
                Status = Status.OnTheWay.ToString()
            };

            List<ShipmentDto> testShipments = new List<ShipmentDto>();
            testShipments.Add(testShiment1);
            testShipments.Add(testShiment2);

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var parService = new ParcelService(assertContext);
                var sut = new ShipmentService(assertContext, parService);
                var resultSHipments = sut.GetAll().ToList();

                for (int i = 0; i < resultSHipments.Count; i++)
                {
                    Assert.AreEqual(testShipments[i].Destination, resultSHipments[i].Destination);
                    Assert.AreEqual(testShipments[i].ArrivalDate, resultSHipments[i].ArrivalDate);
                    Assert.AreEqual(testShipments[i].Status, resultSHipments[i].Status);
                }
            }
        }

        [TestMethod]
        public void DeleteShipmentCorrectly()
        {
            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var parService = new ParcelService(assertContext);
                var sut = new ShipmentService(assertContext, parService);
                sut.Delete(2);
                Assert.ThrowsException<EntityNotFoundException>(() => sut.GetById(2));
                Assert.ThrowsException<EntityNotFoundException>(() => sut.Delete(3));
            }
        }

        [TestMethod]
        public void ThrowException_When_ShipmentNotFound()
        {
            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var parService = new ParcelService(assertContext);
                var sut = new ShipmentService(assertContext, parService);
                int nonExistentId = int.MaxValue;

                Assert.ThrowsException<EntityNotFoundException>(() => sut.GetById(nonExistentId));
            }
        }

        [TestMethod]
        public void PostCreatesCorrectly()
        {
            // Arrange
            var parcel1 = new Parcel
            {
                ParcelId = 1,
                WarehouseId = 1,
                Weight = 4,
                CategoryId = 1,
                CustomerId = 1,
                HomeDelivery = false,
                ShipmentId = 1
            };
            var parcel2 = new Parcel
            {
                ParcelId = 2,
                WarehouseId = 2,
                Weight = 2,
                CategoryId = 2,
                CustomerId = 2,
                HomeDelivery = true,
                ShipmentId = 1
            };

            var Parcels = new List<Parcel>() { parcel1, parcel2 };


            var Shipment3 = new Shipment()
            {
                ShipmentId = 3,
                parcels = Parcels,
                OriginWarehouseId = 1,
                DestinationWarehouseId = 1,
                DepartureDate = DateTime.Now.AddDays(-1),
                ArrivalDate = DateTime.Now.AddDays(2),
                Status = Status.Completed
            };

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var parService = new ParcelService(assertContext);
                var sut = new ShipmentService(assertContext, parService);
                var resultShipment = sut.Create(Shipment3);
                //var expected = sut.GetById(3);

                Assert.AreEqual(resultShipment.ShipmentId, Shipment3.ShipmentId);
                Assert.AreEqual(resultShipment.parcels, Shipment3.parcels);
                Assert.AreEqual(resultShipment.OriginWarehouseId, Shipment3.OriginWarehouseId);
                Assert.AreEqual(resultShipment.DestinationWarehouseId, Shipment3.DestinationWarehouseId);
                Assert.AreEqual(resultShipment.DepartureDate, Shipment3.DepartureDate);
                Assert.AreEqual(resultShipment.ArrivalDate, Shipment3.ArrivalDate);
                Assert.AreEqual(resultShipment.Status, Shipment3.Status);

                //Assert.AreEqual(expected, Customer3);
            }
        }

        [TestMethod]
        public void UpdateShipmentCorrectly()
        {
            // Arrange
            var parcel1 = new Parcel
            {
                ParcelId = 1,
                WarehouseId = 1,
                Weight = 4,
                CategoryId = 1,
                CustomerId = 1,
                HomeDelivery = false,
                ShipmentId = 1
            };
            var parcel2 = new Parcel
            {
                ParcelId = 2,
                WarehouseId = 2,
                Weight = 2,
                CategoryId = 2,
                CustomerId = 2,
                HomeDelivery = true,
                ShipmentId = 1
            };
            var Parcels = new List<Parcel>() { parcel1, parcel2 };

            var Shipment1 = new Shipment()
            {
                ShipmentId = 1,
                parcels = Parcels.Take(2).ToList(),
                OriginWarehouseId = 2,
                DestinationWarehouseId = 2,
                DepartureDate = DateTime.Now,
                ArrivalDate = DateTime.Now.AddDays(2),
                Status = Status.Preparing
            };


            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var parService = new ParcelService(assertContext);
                var sut = new ShipmentService(assertContext, parService);
                var resultShipment = sut.Update(1, Shipment1);
                //var expected = sut.GetById(3);

                Assert.AreEqual(resultShipment.ShipmentId, Shipment1.ShipmentId);
                Assert.AreEqual(resultShipment.parcels, Shipment1.parcels);
                Assert.AreEqual(resultShipment.OriginWarehouseId, Shipment1.OriginWarehouseId);
                Assert.AreEqual(resultShipment.DestinationWarehouseId, Shipment1.DestinationWarehouseId);
                Assert.AreEqual(resultShipment.DepartureDate, Shipment1.DepartureDate);
                Assert.AreEqual(resultShipment.ArrivalDate, Shipment1.ArrivalDate);
                Assert.AreEqual(resultShipment.Status, Shipment1.Status);

                //Assert.AreEqual(expected, Customer3);
            }
        }
        [TestMethod]
        public void FilterByWarehouse()
        {
            // Arrange        
            var testShiment = new ShipmentDto
            {
                Destination = "Baba Tonka 32, Montana, Bulgaria",
                ArrivalDate = DateTime.Now.AddDays(3).ToShortDateString(),
                Status = Status.OnTheWay.ToString()
            };

            List<ShipmentDto> expectedIncoming = new List<ShipmentDto>() { testShiment };


            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var parService = new ParcelService(assertContext);
                var sut = new ShipmentService(assertContext, parService);
                var resultShipment = sut.FilterWarehouse(2).ToList();

                Assert.AreEqual(expectedIncoming[0].Destination, resultShipment[0].Destination);
                Assert.AreEqual(expectedIncoming[0].ArrivalDate, resultShipment[0].ArrivalDate);
                Assert.AreEqual(expectedIncoming[0].Status, resultShipment[0].Status);
            }
        }

        [TestMethod]
        public void FilterByCustomer()
        {
            // Arrange        
            var testShiment1 = new ShipmentDto
            {
                Destination = "Patlajan 32, Montana, Bulgaria",
                ArrivalDate = DateTime.Now.AddDays(1).ToShortDateString(),
                Status = Status.Preparing.ToString()
            }; var testShiment2 = new ShipmentDto
            {
                Destination = "Baba Tonka 32, Montana, Bulgaria",
                ArrivalDate = DateTime.Now.AddDays(3).ToShortDateString(),
                Status = Status.OnTheWay.ToString()
            };

            List<ShipmentDto> expectedIncoming = new List<ShipmentDto>() { testShiment2, testShiment1 };


            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var parService = new ParcelService(assertContext);
                var sut = new ShipmentService(assertContext, parService);
                var resultShipment = sut.GetByCustomer(1).ToList();

                Assert.AreEqual(expectedIncoming[0].Destination, resultShipment[0].Destination);
                Assert.AreEqual(expectedIncoming[0].ArrivalDate, resultShipment[0].ArrivalDate);
                Assert.AreEqual(expectedIncoming[0].Status, resultShipment[0].Status);
            }
        }
    }
}

