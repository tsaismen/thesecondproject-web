﻿using DeliverIt.Data;
using DeliverIT.Models;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Exceptions;
using DeliverIT.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Tests.Services
{
    [TestClass]
    public class Parcel_Should : BaseTest
    {
        private DbContextOptions<DeliveryDbContext> options;

        [TestInitialize]
        public void Initialize()
        {
            this.options = Utils.GetOptions(nameof(TestContext.TestName));

            using (var arrangeContext = new DeliveryDbContext(this.options))
            {
                arrangeContext.Parcels.AddRange(Utils.GetParcels());
                arrangeContext.Cities.AddRange(Utils.GetCities());
                arrangeContext.Countries.AddRange(Utils.GetCountries());
                arrangeContext.Categories.AddRange(Utils.GetCategories());
                arrangeContext.Customers.AddRange(Utils.GetCustomers());
                arrangeContext.Shipments.AddRange(Utils.GetShipments());
                arrangeContext.Warehouses.AddRange(Utils.GetWarehouses());
                arrangeContext.SaveChanges();
            }
        }

        [TestMethod]
        public void ReturnCorrectParcelById()
        {
            // Arrange        
            var testParcel = new ParcelDto
            {
                CustomerName = "Ivailo Petrov",
                Category = "Electronics",
                Weight = 4,
                Status = "Preparing",
                ArrivalDate = "2020-20-13"
            };

            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new ParcelService(assertContext);
                var resultParcel = sut.Get(1);

                Assert.AreEqual(testParcel.CustomerName, resultParcel.Customer.FirstName + " " + resultParcel.Customer.LastName);
                Assert.AreEqual(testParcel.Category, resultParcel.Category.Name);
                Assert.AreEqual(testParcel.Weight, resultParcel.Weight);
                Assert.AreEqual(testParcel.Status, resultParcel.Shipment.Status.ToString());
            }

        }
        [TestMethod]
        public void ThrowExceptionWhenParcelIdIsWrong()
        {
            // Act & Assert
            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new ParcelService(assertContext);
                int nonExistentId = 10;

                Assert.ThrowsException<EntityNotFoundException>(() => sut.Get(nonExistentId));
            }
        }

        [TestMethod]
        public void ReturnAllParcels()
        {
            var testParcels = new List<ParcelDto>() {
                new ParcelDto
                {
                    CustomerName = "Ivailo Petrov",
                    Category = "Electronics",
                    Weight = 4,
                    Status = "Preparing",
                    ArrivalDate = "2020-20-13"
                },
                new ParcelDto
                {
                    CustomerName = "Stanko Petrov",
                    Category = "Food",
                    Weight = 2,
                    Status = "Preparing",
                    ArrivalDate = "2020-20-13"
                },
                new ParcelDto
                {
                    CustomerName = "Ivailo Petrov",
                    Category = "Clothes",
                    Weight = 5,
                    Status = "OnTheWay",
                    ArrivalDate = "2020-20-13"
                },
                new ParcelDto
                {
                    CustomerName = "Stanko Petrov",
                    Category = "Food",
                    Weight = 6,
                    Status = "OnTheWay",
                    ArrivalDate = "2020-20-13"
                }
            };

            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new ParcelService(assertContext);
                var resultParcel = sut.GetAll().ToList();

                for (var i = 0; i < resultParcel.Count; i++)
                {
                    Assert.AreEqual(testParcels[i].CustomerName, resultParcel[i].CustomerName);
                    Assert.AreEqual(testParcels[i].Category, resultParcel[i].Category);
                    Assert.AreEqual(testParcels[i].Weight, resultParcel[i].Weight);
                    Assert.AreEqual(testParcels[i].Status, resultParcel[i].Status.ToString());
                }
            }
        }
        [TestMethod]
        public void SholdCreateParcel()
        {
            // Arrange        
            var testParcel = new Parcel
            {
                ParcelId = 5,
                WarehouseId = 1,
                Weight = 7,
                CategoryId = 1,
                CustomerId = 1,
                HomeDelivery = false,
                ShipmentId = 1
            };
            var testPrevousParcel = new Parcel
            {
                ParcelId = 5,
                WarehouseId = 1,
                Weight = 7,
                CategoryId = 1,
                CustomerId = 1,
                HomeDelivery = false,
                ShipmentId = 1
            };
            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new ParcelService(assertContext);
                var newParcel = sut.Create(testParcel);
                var resultParcel = sut.Get(5);

                Assert.AreEqual(testParcel.Customer.FirstName + " " + resultParcel.Customer.LastName, resultParcel.Customer.FirstName + " " + resultParcel.Customer.LastName);
                Assert.AreEqual(testParcel.Category.Name, resultParcel.Category.Name);
                Assert.AreEqual(testParcel.Weight, resultParcel.Weight);
                Assert.AreEqual(testParcel.Shipment.Status.ToString(), resultParcel.Shipment.Status.ToString());
                Assert.ThrowsException<DuplicateEntityException>(() => sut.Create(testPrevousParcel));
            }

        }

        [TestMethod]
        public void ShouldUpdateParcel()
        {
            // Arrange        
            var testParcel = new Parcel
            {
                ParcelId = 5,
                WarehouseId = 1,
                Weight = 7,
                CategoryId = 1,
                CustomerId = 1,
                HomeDelivery = false,
                ShipmentId = 1
            };
            var testPrevousParcel = new Parcel
            {
                ParcelId = 5,
                WarehouseId = 1,
                Weight = 10,
                CategoryId = 2,
                CustomerId = 1,
                HomeDelivery = false,
                ShipmentId = 1
            };
            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new ParcelService(assertContext);
                var newParcel = sut.Create(testParcel);
                sut.Update(5, testPrevousParcel);
                var resultParcel = sut.Get(5);

                Assert.AreEqual(10, resultParcel.Weight);
                Assert.AreEqual("Food", resultParcel.Category.Name);
                Assert.AreEqual("Preparing", resultParcel.Shipment.Status.ToString());
            }
        }
        [TestMethod]
        public void ShouldDelete()
        {
            // Arrange        
            var testParcel = new Parcel
            {
                ParcelId = 5,
                WarehouseId = 1,
                Weight = 7,
                CategoryId = 1,
                CustomerId = 1,
                HomeDelivery = false,
                ShipmentId = 1
            };
            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new ParcelService(assertContext);
                var newParcel = sut.Create(testParcel);
                sut.Delete(5);

                Assert.ThrowsException<EntityNotFoundException>(() => sut.Get(5));
                Assert.ThrowsException<EntityNotFoundException>(() => sut.Delete(5));
            }
        }

        [TestMethod]
        public void ShouldFilterProperly()
        {
            // Arrange        
            var testParcels = new List<ParcelDto>() {
                new ParcelDto
                {
                    CustomerName = "Ivailo Petrov",
                    Category = "Electronics",
                    Weight = 4,
                    Status = "Preparing",
                    ArrivalDate = "2020-20-13"
                },
                new ParcelDto
                {
                    CustomerName = "Stanko Petrov",
                    Category = "Food",
                    Weight = 2,
                    Status = "Preparing",
                    ArrivalDate = "2020-20-13"
                },
                new ParcelDto
                {
                    CustomerName = "Ivailo Petrov",
                    Category = "Clothes",
                    Weight = 5,
                    Status = "OnTheWay",
                    ArrivalDate = "2020-20-13"
                },
                new ParcelDto
                {
                    CustomerName = "Stanko Petrov",
                    Category = "Food",
                    Weight = 6,
                    Status = "OnTheWay",
                    ArrivalDate = "2020-20-13"
                }
            };
            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new ParcelService(assertContext);
                var fitleredParcels = sut.Filter(1, "Electronics", 1, 4).ToList();

                Assert.AreEqual(fitleredParcels.Count(), 1);
                Assert.AreEqual(testParcels[0].Weight, fitleredParcels[0].Weight);
            }
        }
        [TestMethod]
        public void ShouldSortProperly()
        {
            // Arrange        
            var testParcels = new List<ParcelDto>() {
                new ParcelDto
                {
                    CustomerName = "Ivailo Petrov",
                    Category = "Electronics",
                    Weight = 4,
                    Status = "Preparing",
                    ArrivalDate = "2020-20-13"
                },
                new ParcelDto
                {
                    CustomerName = "Stanko Petrov",
                    Category = "Food",
                    Weight = 2,
                    Status = "Preparing",
                    ArrivalDate = "2020-20-13"
                },
                new ParcelDto
                {
                    CustomerName = "Ivailo Petrov",
                    Category = "Clothes",
                    Weight = 5,
                    Status = "OnTheWay",
                    ArrivalDate = "2020-20-13"
                },
                new ParcelDto
                {
                    CustomerName = "Stanko Petrov",
                    Category = "Food",
                    Weight = 6,
                    Status = "OnTheWay",
                    ArrivalDate = "2020-20-13"
                }
            };
            // Act & Assert
            using (var assertContext = new DeliveryDbContext(this.options))
            {
                var sut = new ParcelService(assertContext);
                var fitleredParcelsByWeightDesc = sut.SortBy("desc", "asc").ToList();
                for (var i = 0; i < fitleredParcelsByWeightDesc.Count(); i++)
                {
                    Assert.AreEqual(6, fitleredParcelsByWeightDesc[i].Weight);
                    Assert.AreEqual(5, fitleredParcelsByWeightDesc[i].Weight);
                    Assert.AreEqual(4, fitleredParcelsByWeightDesc[i].Weight);
                    Assert.AreEqual(2, fitleredParcelsByWeightDesc[i].Weight);

                    var fitleredParcelsByWeightAsc = sut.SortBy("asc", "asc").ToList();
                    Assert.AreEqual(2, fitleredParcelsByWeightAsc[i].Weight);
                    Assert.AreEqual(4, fitleredParcelsByWeightAsc[i].Weight);
                    Assert.AreEqual(5, fitleredParcelsByWeightAsc[i].Weight);
                    Assert.AreEqual(6, fitleredParcelsByWeightAsc[i].Weight);

                    var fitleredParcelsByArrivalDesc = sut.SortBy("desc", "desc").ToList();
                    Assert.AreEqual(6, fitleredParcelsByArrivalDesc[i].Weight);
                    Assert.AreEqual(5, fitleredParcelsByArrivalDesc[i].Weight);
                    Assert.AreEqual(4, fitleredParcelsByArrivalDesc[i].Weight);
                    Assert.AreEqual(2, fitleredParcelsByArrivalDesc[i].Weight);

                    var fitleredParcelsByArrivalAsc = sut.SortBy("asc", "desc").ToList();
                    Assert.AreEqual(2, fitleredParcelsByArrivalAsc[i].Weight);
                    Assert.AreEqual(4, fitleredParcelsByArrivalAsc[i].Weight);
                    Assert.AreEqual(5, fitleredParcelsByArrivalAsc[i].Weight);
                    Assert.AreEqual(6, fitleredParcelsByArrivalAsc[i].Weight);

                    var fitleredParcelsByArrivalDesc1 = sut.SortBy("desc", null).ToList();
                    Assert.AreEqual(6, fitleredParcelsByArrivalDesc1[i].Weight);
                    Assert.AreEqual(5, fitleredParcelsByArrivalDesc1[i].Weight);
                    Assert.AreEqual(4, fitleredParcelsByArrivalDesc1[i].Weight);
                    Assert.AreEqual(2, fitleredParcelsByArrivalDesc1[i].Weight);

                    var fitleredParcelsByArrivalAsc1 = sut.SortBy("asc", null).ToList();
                    Assert.AreEqual(2, fitleredParcelsByArrivalAsc1[i].Weight);
                    Assert.AreEqual(4, fitleredParcelsByArrivalAsc1[i].Weight);
                    Assert.AreEqual(5, fitleredParcelsByArrivalAsc1[i].Weight);
                    Assert.AreEqual(6, fitleredParcelsByArrivalAsc1[i].Weight);

                    var fitleredParcelsByArrivalDesc2 = sut.SortBy(null, "asc").ToList();
                    Assert.AreEqual(4, fitleredParcelsByArrivalDesc2[i].Weight);
                    Assert.AreEqual(2, fitleredParcelsByArrivalDesc2[i].Weight);
                    Assert.AreEqual(5, fitleredParcelsByArrivalDesc2[i].Weight);
                    Assert.AreEqual(6, fitleredParcelsByArrivalDesc2[i].Weight);

                    var fitleredParcelsByArrivalAsc2 = sut.SortBy(null, "desc").ToList();
                    Assert.AreEqual(2, fitleredParcelsByArrivalAsc[i].Weight);
                    Assert.AreEqual(4, fitleredParcelsByArrivalAsc[i].Weight);
                    Assert.AreEqual(5, fitleredParcelsByArrivalAsc[i].Weight);
                    Assert.AreEqual(6, fitleredParcelsByArrivalAsc[i].Weight);

                    Assert.ThrowsException<InvalidInputDataException>(() => sut.SortBy(null, null));
                    Assert.ThrowsException<InvalidInputDataException>(() => sut.SortBy("test", "sort"));
                    Assert.ThrowsException<InvalidInputDataException>(() => sut.SortBy("test", null));
                    Assert.ThrowsException<InvalidInputDataException>(() => sut.SortBy(null, "sort"));
                }
            }
        }
    }
}
