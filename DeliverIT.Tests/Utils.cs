using DeliverIt.Data;
using DeliverIT.Models;
using DeliverIT.Models.AddressFolder;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace DeliverIT.Tests
{

    public class Utils
    {
        public static DbContextOptions<DeliveryDbContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<DeliveryDbContext>()
                       .UseInMemoryDatabase(databaseName)
                       .Options;
        }

        public static IEnumerable<Address> GetAddresses()
        {
            return ModelBuilderExtension.Addresses;
        }
        public static IEnumerable<City> GetCities()
        {
            return ModelBuilderExtension.Cities;
        }
        public static IEnumerable<Country> GetCountries()
        {
            return ModelBuilderExtension.Countries;
        }
        public static IEnumerable<Customer> GetCustomers()
        {
            return ModelBuilderExtension.Customers;
        }
        public static IEnumerable<Employee> GetEmployees()
        {
            return ModelBuilderExtension.Employees;
        }
        public static IEnumerable<Category> GetCategories()
        {
            return ModelBuilderExtension.Categories;
        }
        public static IEnumerable<Warehouse> GetWarehouses()
        {
            return ModelBuilderExtension.Warehouses;
        }
        public static IEnumerable<Parcel> GetParcels()
        {
            return ModelBuilderExtension.Parcels;
        }
        public static IEnumerable<Shipment> GetShipments()
        {
            return ModelBuilderExtension.Shipments;
        }
    }
}
