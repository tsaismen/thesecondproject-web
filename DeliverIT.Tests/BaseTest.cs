﻿using DeliverIt.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DeliverIT.Tests
{
    public abstract class BaseTest
    {
        [TestCleanup()]
        public void Cleanup()
        {
            var options = Utils.GetOptions(nameof(TestContext.TestName));
            var context = new DeliveryDbContext(options);
            context.Database.EnsureDeleted();
        }
    }
}
